
# Decorators

## Introduction

A decorator is the name used for a software design pattern. Decorators dynamically alter the functionality of a function, method, or class without having to directly use subclasses or change the source code of the function being decorated.

Python decorator is a specific change to the Python syntax that allows us to more conveniently alter functions and methods (and possibly classes in a future version). This supports more readable applications of the DecoratorPattern but also other uses as well.


```python
def bread(test_funct):
    def hyderabad():
        print("</''''''\>")
        test_funct()
        print("<\______/>")
    return hyderabad

def ingredients(test_funct):
    def chennai():
        print("#tomatoes#")
        test_funct()
        print("~salad~")
    return chennai

def cheese(food="--Say Cheese--"):
    print(food)

```


```python
ch = bread(test_funct=cheese)
print(ch)
```

    <function bread.<locals>.hyderabad at 0x000000000529F620>



```python
ch()
```

    </''''''\>
    --Say Cheese--
    <\______/>



```python
inn = bread(ingredients(cheese))
inn()
```

    </''''''\>
    #tomatoes#
    --Say Cheese--
    ~salad~
    <\______/>


## Function Decorators

A function decorator is applied to a function definition by placing it on the line before that function definition begins


```python
@bread
@ingredients
def sandwich(food="--Say Cheese--"):
    print(food)

sandwich()
```

    </''''''\>
    #tomatoes#
    --Say Cheese--
    ~salad~
    <\______/>


> ***!!! Order Matters !!!*** 


```python
@ingredients
@bread
def sandwich(food="--Say Cheese--"):
    print(food)

sandwich()
```

    #tomatoes#
    </''''''\>
    --Say Cheese--
    <\______/>
    ~salad~



```python
@bread
@ingredients
def hotdog(food="Jam"):
    print(food)

hotdog()
```

    </''''''\>
    #tomatoes#
    Jam
    ~salad~
    <\______/>



```python
def diet_sandwitch(inner_func):
    def inner():
        print("salad")
    return inner
```


```python
@ingredients
@diet_sandwitch
def sandwich(food="--Say Cheese--"):
    print(food)

sandwich()
```

    #tomatoes#
    salad
    ~salad~


## Decorators with arguments

### Using Functions

We can create a function which can take an argument and wrap the original decorator inside as shown in below code sample.


```python
def decorator(*deco_args, **deco_kwargs):
    def real_decorator(func):
        def inner(*args, **kwargs):
            print("inside inner args:", deco_args)
            for k, v in deco_kwargs.items():
                print("inside inner kwars:", k, v)
            func(*args, **kwargs)
        return inner
    return real_decorator


@decorator("arg1", "arg2")
def print_args(*args, **kwargs):
    print("TEST")
    for arg in args:
        print("arg", arg)
    for k, v in kwargs.items():
        print("kwargs", k, v)
```


```python
print_args(1, 2, 3, corp="Bhopal Municipal Corporation")
```

    inside inner args: ('arg1', 'arg2')
    TEST
    arg 1
    arg 2
    arg 3
    kwargs corp Bhopal Municipal Corporation



```python
@decorator("arg1", "arg2", ver="10.2.301")
def print_args(*args, **kwargs):
    print("TEST")
    for arg in args:
        print("arg", arg)
    for k, v in kwargs.items():
        print("kwargs", k, v)
```


```python
print_args(1, 2, 3, corp="Bhopal Municipal Corporation")
```

    inside inner args: ('arg1', 'arg2')
    inside inner kwars: ver 10.2.301
    TEST
    arg 1
    arg 2
    arg 3
    kwargs corp Bhopal Municipal Corporation



```python
def decorator(*deco_args, **deco_kwargs):
    def real_decorator(func):
        def inner(*args, **kwargs):
            print("inside inner args:", args)
            print("inside inner kwargs:", kwargs)
            # Calling the actual function
            func(*args, **kwargs)
        return inner
    return real_decorator


@decorator("arg1", "arg2")
def print_args(*args, **kwargs):
    print("TEST")
    for arg in args:
        print("arg", arg)
    for k, v in kwargs.items():
        print("kwargs", k, v)
```


```python
print_args(1, 2, 3, corp="Bhopal Municipal Corporation")
```

    inside inner args: (1, 2, 3)
    inside inner kwargs: {'corp': 'Bhopal Municipal Corporation'}
    TEST
    arg 1
    arg 2
    arg 3
    kwargs corp Bhopal Municipal Corporation



```python
def decorator(*deco_args, **deco_kwargs):
    def real_decorator(func):
        def inner(*args, **kwargs):
            print("inside inner args:", args)
            print("inside inner kwargs:", kwargs)
            # Calling the actual function
            if args[0] == 0:
                func(*args, **kwargs)
        return inner
    return real_decorator


@decorator("arg1", "arg2")
def print_args(*args, **kwargs):
    print("TEST")
    for arg in args:
        print("arg", arg)
    for k, v in kwargs.items():
        print("kwargs", k, v)
```


```python
print_args(1, 2, 3, corp="Bhopal Municipal Corporation")
```

    inside inner args: (1, 2, 3)
    inside inner kwargs: {'corp': 'Bhopal Municipal Corporation'}



```python
print_args(0, 1, 2, 3, corp="Bhopal Municipal Corporation")
```

    inside inner args: (0, 1, 2, 3)
    inside inner kwargs: {'corp': 'Bhopal Municipal Corporation'}
    TEST
    arg 0
    arg 1
    arg 2
    arg 3
    kwargs corp Bhopal Municipal Corporation


In the above example, we bypassed the execution of actual function itself.

### Limitations 

$$TODO$$

We can obtain the same using the `wrapper` function of functools


```python
from functools import wraps

def decorator(*deco_args, **deco_kwargs):
    method_or_name = deco_args
    
    def real_decorator(method):    
        if callable(method_or_name):
            print("if", method_or_name)
            method.gw_method = method.__name__
        else:
            print("Else", method_or_name)
            method.gw_method = method_or_name

        @wraps(method)
        def wrapper(*args, **kwargs):
            method(*args, **kwargs)
        return wrapper
    if callable(method_or_name):
        return real_decorator(method_or_name)
    return real_decorator

@decorator("arg1", "arg2", test="test")
def print_args(*args, **kwargs):
    for arg in args:
        print(arg)
    for k, v in kwargs.items():
        print(k, v)

print_args(1, 2, 3, d="Satyam")
```

    Else ('arg1', 'arg2')
    1
    2
    3
    d Satyam


#### Conditional Decorators

There are scenarios were we want to run the decorators only under certain condition, although Python do not natively support it, but we can achieve it with the following code template.


```python
def condition(**deco_kwargs):
    
    def __decorator__(func):
    
        def __inner__(*args, **kwargs):
            if deco_kwargs.get('flag', False):
                return func(*args, **kwargs)
            
        return __inner__
    return __decorator__
        

def decorator(func):
    def inner(*args, **kwargs):
        print("Yadayada _ in inner")
        print(*args, **kwargs)
        print(args)
        for k, v in kwargs.items():
            print(k, v)
        func(*args, **kwargs)
    return inner

debug = False

@condition(flag=debug)
@decorator
def print_args(*args, **kwargs):
    for arg in args:
        print(arg)
    for k, v in kwargs.items():
        print(k, v)


print_args(1, 2)
print("...done....")
```

    ...done....



```python
# class condition(object):
#     def __init__(self, decorator, condition):
#         self.decorator = decorator
#         self.condition = condition

#     def __call__(self, func):
#         if self.condition:
#             return self.decorator(func)
#         return func


def condition(**deco_kwargs):
    def __decorator__(func):
        def __inner__(*args, **kwargs):
            if deco_kwargs.get('flag', False):
                return func(*args, **kwargs)
#             return func(*args, **kwargs)
        return __inner__
    return __decorator__
        

def decorator(func):
    def inner(*args, **kwargs):
        print("Yadayada _ in inner")
        print(*args, **kwargs)
        print(args)
        for k, v in kwargs.items():
            print(k, v)
        func(*args, **kwargs)
    return inner

@condition(flag=debug)
@decorator
def print_args(*args, **kwargs):
    for arg in args:
        print(arg)
    for k, v in kwargs.items():
        print(k, v)

debug = True
print_args(1, 2)
```


    ---------------------------------------------------------------------------

    NameError                                 Traceback (most recent call last)

    <ipython-input-1-6fc2290b19a7> in <module>()
         30     return inner
         31 
    ---> 32 @condition(flag=debug)
         33 @decorator
         34 def print_args(*args, **kwargs):


    NameError: name 'debug' is not defined


### Context Managers as Decorators 

Context managers can also be used as decorators, as shown below (https://docs.python.org/dev/library/contextlib.html#contextlib.ContextDecorator)


```python
from contextlib import ContextDecorator

class mycontext(ContextDecorator):
    def __enter__(self):
        print('Starting')
        return self

    def __exit__(self, *exc):
        print('Finishing')
        return False

@mycontext()
def function():
    print('The bit in the middle')
```


```python
function()
```

    Starting
    The bit in the middle
    Finishing



```python
def function_2():
    print("This is function 2")
    
with mycontext() as t: 
    function_2()
```

    Starting
    This is function 2
    Finishing


## Class Decorators

## Bound methods
---

Unless you tell it not to, Python will create what is called a bound method when a function is an attribute of a class and you access it via an instance of a class. This may sound complicated but it does exactly what you want.


```python
class A(object):
    def method(*argv):
        return argv
a = A()
a.method

```




    <bound method A.method of <__main__.A object at 0x00000234D70DB8D0>>




```python
a.method('an arg')
```




    (<__main__.A at 0x234d70db8d0>, 'an arg')



### staticmethod()

A static method is a way of suppressing the creation of a bound method when accessing a function.


```python
class A(object):
    @staticmethod
    def method(*argv):
        return argv
a = A()
a.method
```




    <function __main__.A.method>



When we call a static method we don’t get any additional arguments.


```python
a.method('an arg')
```




    ('an arg',)



### classmethod

A class method is like a bound method except that the class of the instance is passed as an argument rather than the instance itself.


```python
class A(object):
    @classmethod
    def method(*argv):
        return argv
a = A()
a.method
```




    <bound method type.method of <class '__main__.A'>>




```python
a.method('an arg')
```




    (__main__.A, 'an arg')




```python
def test(strg):
    print("Name: ", strg)
    
def hello(func, name):
    print("Ja")
    func(name)
    
hello(test, "Mayank")
```

    Ja
    Name:  Mayank



```python
class B(object):
    @classmethod
    def method(*argv):
        return argv
```


```python
a = B()
a.method()
```


    ---------------------------------------------------------------------------

    NameError                                 Traceback (most recent call last)

    <ipython-input-2-9efc1235472a> in <module>()
    ----> 1 a = B()
          2 a.method()


    NameError: name 'B' is not defined


## References:

- https://www.python.org/dev/peps/pep-0318/
