
## bytes

The Syntax of `bytes` are same as that for `string`, except `b` prefix in `bytes` as shown in the below examples


```python
s1 = b'This is good'
s2 = b'This is "good"'
s3 = b'''This is, \t
really good'''
print(s1, s2, s3)
```

    b'This is good' b'This is "good"' b'This is, \t\nreally good'


> **<center>NOTE</center>**
><hr>
> Only ASCII characters are permitted in bytes literals and any binary values over 127 must be entered into bytes literals using the appropriate escape sequence.
>
> **Example:**
>```python
    s_hindi = b'हल'
```
**Output:**
```python
  File "<ipython-input-9-ddcc7b258ee9>", line 2
    s_hindi = b'हल'
             ^
SyntaxError: bytes can only contain ASCII literal characters.
```

Similar to Strings bytes can also use `r` prefix which disables processing of escape sequences.


```python
s3 = br'''This is, \t
really good'''

print(s3)
```

    b'This is, \\t\nreally good'


`Bytes` objects behave similar to immutable sequences of integers with values between `0<=x <256`, as shown in the below example.


```python
for a in s3:
    print(a, end=", ")
```

    84, 104, 105, 115, 32, 105, 115, 44, 32, 92, 116, 10, 114, 101, 97, 108, 108, 121, 32, 103, 111, 111, 100, 

`bytes` can also be created using one of the following methods


```python
# A zero-filled bytes object of a specified length
print(bytes(5))

# From an iterable of integers
print(bytes(range(10)))

# copying existing binary data
b = [123, 131, 111, 121]
print(bytes(b))
```

    b'\x00\x00\x00\x00\x00'
    b'\x00\x01\x02\x03\x04\x05\x06\x07\x08\t'
    b'{\x83oy'

