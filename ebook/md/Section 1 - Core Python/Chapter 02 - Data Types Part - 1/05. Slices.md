
## Slices

*Slices* of *strings* can be obtained by adding indexes between brackets after a  *string* or any other iterable object.

![Slicing strings](files/String_slicing_1.png)

### Python indexes:
-------

+ Start with zero.
+ Count from the end if they are negative.
+ Can be defined as sections, in the form `[start: end + 1: step]`. If not set the start, it will be considered as zero. If not set end + 1, it will be considered the size of the object. The step (between characters), if not set, is 1.

> **!!! TIP !!!**: 
It is possible to invert *strings* by using a negative step:


```python
p = "Manish"
print(p[0:2]) 
print(p[:2])
print(p[1:4])
print(p[-5:-2])
```

    Ma
    Ma
    ani
    ani



```python
print (p[::-1])
```

    hsinaM



```python
print("implemented"[5:7:-2])
```

    



```python
print("implemented"[::-2])
print("implemented"[-5::-2])
print("implemented"[:-7:-2])
```

    ipeetd
    eepi
    dte



```python
print("implemented"[::2])
print("implemented"[1::2])
print("implemented"[1:4:2]) 
```

    dteepi
    eepi
    dte
    ipeetd
    mlmne
    ml



```python
str1="Guten Tag"
print(str1[1:])
print(str1[-1:])
print(str1[-2:4])
print(str1[-4:-2])
print(str1[-1:2:-1])
print(str1[4])
```

    uten Tag
    g
    
     T
    gaT ne
    n


> ?1 -> starts from i(**m**) and ends


```python
x = 10
print(id(x))
x = "Mayank Johri"
print(x)
print("id(x)",id(x))
print("x[0:]", id(x[0:]))
print(id(x[1:]))
print(id(x[2:]))
print(x[0:])
print(x[1:])
print(x[2])

x = "God knows"
print(x)
print(id(x))
print(id(x[0]))
print(id(x[1]))
print(id(x[2]))
print(x[0])
print(x[1])
print(x[2])
```

    140101306922496
    Mayank Johri
    id(x) 140101036238448
    x[0:] 140101036238448
    140101035786928
    140101035786864
    Mayank Johri
    ayank Johri
    y
    God knows
    140101036236848
    140101287739440
    140101287740896
    140101288140336
    G
    o
    d


### Changing List using slicing

Slicing allows to add/update/remove the elements from a list as shown below

#### Adding elements of list 


```python
lst = [1, 3, 4, 5]

lst[1:2] = [2, 0, 2, 0, 2, 0]
print(lst)
```

    [1, 2, 0, 2, 0, 2, 0, 4, 5]


#### appending elements of list


```python
lst = [1, 2, 3, 4, 5]

lst[1:4] = [234]
print(lst)
```

    [1, 234, 5]


In the above example, elements with index value 1, 2 & 3 were removed and '234' was inserted in their place 

#### Removing elements of list


```python
lst = [1, 2, 3, 4, 5]
del(lst[1:4])
print(lst)
```

    [1, 5]


In the above example, elements with index value 1, 2 & 3 were removed using `del` keyword


```python
lst = [1, 2, 3, 4, 5]

lst[1:4] = []
print(lst)
```

    [1, 5]


In the above example, elements with index value 1, 2 & 3 were removed using `[]`
