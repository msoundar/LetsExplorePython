# Summary

* [README](./README.md)
* \[Chapter 00 - Preface\]
	* [Chapter 0 - Who and Why](./Chapter 00 - Preface/Chapter 0 - Who and Why.md)
* \[Chapter 01 - Introduction\]
	* [01. Introduction](./Chapter 01 - Introduction/01. Introduction.md)
	* [02. Python Installation](./Chapter 01 - Introduction/02. Python Installation.md)
	* [03. Starting Point](./Chapter 01 - Introduction/03. Starting Point.md)
	* [04. Syntax](./Chapter 01 - Introduction/04. Syntax.md)
	* [05. Python Identifiers](./Chapter 01 - Introduction/05. Python Identifiers.md)
	* [06. Python Naming Conventions](./Chapter 01 - Introduction/06. Python Naming Conventions.md)
	* [07. reserved_keywords](./Chapter 01 - Introduction/07. reserved_keywords.md)
	* [08. Python2 vs Python3](./Chapter 01 - Introduction/08. Python2 vs Python3.md)
* \[Chapter 02 - Basic Data Types\]
	* [01. Introduction](./Chapter 02 - Basic Data Types/01. Introduction.md)
	* [02. Numbers](./Chapter 02 - Basic Data Types/02. Numbers.md)
	* [03. Boolean](./Chapter 02 - Basic Data Types/03. Boolean.md)
	* [04. String](./Chapter 02 - Basic Data Types/04. String.md)
	* [04_01. String Operations](./Chapter 02 - Basic Data Types/04_01. String Operations.md)
	* [04_02. String Module](./Chapter 02 - Basic Data Types/04_02. String Module.md)
	* [04_03. bytes](./Chapter 02 - Basic Data Types/04_03. bytes.md)
	* [04_04. bytearrays](./Chapter 02 - Basic Data Types/04_04. bytearrays.md)
	* [04_05_String Conversion](./Chapter 02 - Basic Data Types/04_05_String Conversion.md)
	* [05. Slices](./Chapter 02 - Basic Data Types/05. Slices.md)
	* [06. Lists](./Chapter 02 - Basic Data Types/06. Lists.md)
	* [07. Dictionary](./Chapter 02 - Basic Data Types/07. Dictionary.md)
	* [08. Operators](./Chapter 02 - Basic Data Types/08. Operators.md)
	* [08_01. Exercises](./Chapter 02 - Basic Data Types/08_01. Exercises.md)
	* [09. Answers - Data_Type](./Chapter 02 - Basic Data Types/09. Answers - Data_Type.md)
	* [10. Questions - Dictionary](./Chapter 02 - Basic Data Types/10. Questions - Dictionary.md)
* \[Chapter 03 - Control Flow\]
	* [03_01. Compound Statements](./Chapter 03 - Control Flow/03_01. Compound Statements.md)
	* [03_02. Exercises - Conditions](./Chapter 03 - Control Flow/03_02. Exercises - Conditions.md)
* \[Chapter 04 - Loops\]
	* [04_01. Loops](./Chapter 04 - Loops/04_01. Loops.md)
	* [04_02. Exercise - Loops](./Chapter 04 - Loops/04_02. Exercise - Loops.md)
* \[Chapter 05 - Functions\]
	* [01. Functions](./Chapter 05 - Functions/01. Functions.md)
	* [02. Lambda](./Chapter 05 - Functions/02. Lambda.md)
	* [02. Monkey Patching](./Chapter 05 - Functions/02. Monkey Patching.md)
	* [04. Exercise - Function](./Chapter 05 - Functions/04. Exercise - Function.md)
* \[Chapter 06 - Scope\]
	* [Scope_of_names](./Chapter 06 - Scope/Scope_of_names.md)
* \[Chapter 07 - Modules\]
	* [Modules](./Chapter 07 - Modules/Modules.md)
* \[Chapter 08 - Classes & OOPS\]
	* [01_Classes_and_OOPS](./Chapter 08 - Classes & OOPS/01_Classes_and_OOPS.md)
	* [02_01_Inheritance_Introduction](./Chapter 08 - Classes & OOPS/02_01_Inheritance_Introduction.md)
	* [02_02_Single_Inheritance](./Chapter 08 - Classes & OOPS/02_02_Single_Inheritance.md)
	* [02_03_Multiple_Inheritance](./Chapter 08 - Classes & OOPS/02_03_Multiple_Inheritance.md)
	* [03_Composition](./Chapter 08 - Classes & OOPS/03_Composition.md)
	* [04_Polymorphism](./Chapter 08 - Classes & OOPS/04_Polymorphism.md)
	* [05_Inner_Class](./Chapter 08 - Classes & OOPS/05_Inner_Class.md)
	* [06_ABC](./Chapter 08 - Classes & OOPS/06_ABC.md)
	* [07_Misc](./Chapter 08 - Classes & OOPS/07_Misc.md)
	* [08_MetaProgramming](./Chapter 08 - Classes & OOPS/08_MetaProgramming.md)
	* [09_Python_Descriptors](./Chapter 08 - Classes & OOPS/09_Python_Descriptors.md)
	* [10. _, __ and __attr__ in Python](./Chapter 08 - Classes & OOPS/10. _, __ and __attr__ in Python.md)
	* [11_Hurdles](./Chapter 08 - Classes & OOPS/11_Hurdles.md)
* \[Chapter 09 - Advance Data Types\]
	* [05_01. Advance Data Types](./Chapter 09 - Advance Data Types/05_01. Advance Data Types.md)
	* [05_02. Uses of collection and datatype](./Chapter 09 - Advance Data Types/05_02. Uses of collection and datatype.md)
	* [05_03. Other types of sequences](./Chapter 09 - Advance Data Types/05_03. Other types of sequences.md)
	* [05_04_Advance String](./Chapter 09 - Advance Data Types/05_04_Advance String.md)
	* [05_05. Excercise](./Chapter 09 - Advance Data Types/05_05. Excercise.md)
* \[Chapter 10 - Generators\]
	* [01_Generators](./Chapter 10 - Generators/01_Generators.md)
	* [02_Iterators](./Chapter 10 - Generators/02_Iterators.md)
* \[Chapter 11 - Exceptions\]
	* [01_Exceptions](./Chapter 11 - Exceptions/01_Exceptions.md)
* \[Chapter 12 - Introspection\]
	* [Chapter14_Introspection](./Chapter 12 - Introspection/Chapter14_Introspection.md)
* \[Chapter 13 - Decorators\]
	* [Decorators](./Chapter 13 - Decorators/Decorators.md)
* \[Chapter 14 - Properties\]
	* [property](./Chapter 14 - Properties/property.md)
* \[Chapter 15 - Files and IO\]
	* [01. Files and IO](./Chapter 15 - Files and IO/01. Files and IO.md)
* \[Chapter 16 - Standard library\]
	* [01_Standard_library](./Chapter 16 - Standard library/01_Standard_library.md)
	* [02_Reference, Shallow and deep copy](./Chapter 16 - Standard library/02_Reference, Shallow and deep copy.md)
	* [03_Random](./Chapter 16 - Standard library/03_Random.md)
	* [04_print](./Chapter 16 - Standard library/04_print.md)
* \[Chapter 17 - PIP and DistUtils\]
	* [Python Package Management](./Chapter 17 - PIP and DistUtils/Python Package Management.md)
* \[Chapter 19 - Custom Data Types\]
	* [Custom Datatypes](./Chapter 19 - Custom Data Types/Custom Datatypes.md)
* \[Chapter 20 - Debugging\]
	* [1. Introduction](./Chapter 20 - Debugging/1. Introduction.md)
	* [2. Static Code Analyser](./Chapter 20 - Debugging/2. Static Code Analyser.md)
	* [3. Debugging](./Chapter 20 - Debugging/3. Debugging.md)
	* [4. faulthandler](./Chapter 20 - Debugging/4. faulthandler.md)
* \[Chapter S1.A - Review & Class work\]
	* [Interview Questions - Core Python](./Chapter S1.A - Review & Class work/Interview Questions - Core Python.md)
* [SUMMARY](./SUMMARY.md)
