
## Chapter 10 : Standard Library



It's often said that Python comes with **"batteries included"**, in reference to the vast library of modules and packages that are distributed with the interpreter.

Some important modules of the standard library:
+ Math: math, cmath, and random decimal.
+ System: os, glob, subprocess and shutils.
+ Threads: threading.
+ Persistence: pickle and cPickle.
+ XML: xml.dom, xml.sax and ElementTree (since version 2.5).
+ Configuration: ConfigParser and optparse.
+ Time: time and datetime.
+ Other: sys, logging, traceback, types and timeit.

## Maths


In addition to the *builtin* numeric types in the Python standard library, there are several modules devoted to implementing other types and mathematical operations.

The *math* module defines logarithmic, exponentiation, trigonometric, and hyperbolic functions, as well as angular conversions and more. The *cmath* module implements similar functions, but can handle complex numbers.

Example:


```python
import math
import cmath

# Complex
for cpx in [3j, 1.5 + 1j, -2 - 2j]:

    # Polar coordinate conversion
    plr = cmath.polar(cpx)
    print ('Complex:', cpx)
    print ('Polar:', plr, '(in radians)')
    print ('Amplitude:', abs(cpx))
    print ('Angle:', math.degrees(plr[1]), '(grades)')
```

    Complex: 3j
    Polar: (3.0, 1.5707963267948966) (in radians)
    Amplitude: 3.0
    Angle: 90.0 (grades)
    Complex: (1.5+1j)
    Polar: (1.8027756377319946, 0.5880026035475675) (in radians)
    Amplitude: 1.8027756377319946
    Angle: 33.690067525979785 (grades)
    Complex: (-2-2j)
    Polar: (2.8284271247461903, -2.356194490192345) (in radians)
    Amplitude: 2.8284271247461903
    Angle: -135.0 (grades)


The *random* module brings functions for random number generation.

Examples:


```python
from random import choice, randrange, random

# Choice returns one value from the list
print (choice(string.ascii_uppercase))
print (choice([10, 200, 399, "Hello", "God", "Python", [222]]))
```

    A
    10



```python
# Choose a float from 0 to 1
print (int(random()*100))
```

    30



```python
# Choose a number from 1 to 10
for x in range(10):
    x = randrange(1, 100)
    print (x, end="   ")
```

    7   87   90   62   47   5   16   60   51   92   


```python
import random
lst = ["a", "b", "c", "e"]
dirs ={}

for  l in lst:
    dirs[l] = random.randrange(1, 100)

print(dirs)
```

    {'a': 74, 'b': 68, 'c': 60, 'e': 16}


In the standard library there is the decimal module that defines operations with real numbers with fixed precision.

Example:


```python
from decimal import Decimal

t = 5.
for i in range(50):
    t = t - 0.1

print ('Float:', t)

t = Decimal('5.')
for i in range(50):
    t = t - Decimal('0.1')

print ('Decimal:', t)
```

    Float: 1.0269562977782698e-15
    Decimal: 0.0


With this module, it is possible to reduce the introduction of rounding errors arising from floating point arithmetic.

In version 2.6, the module *fractions*, which deals with rational numbers,  is also available.

Example:


```python
from fractions import Fraction

# Three fractions
f1 = Fraction('-2/3')
f2 = Fraction(3, 4)
f3 = Fraction('.25')
print ("Fraction('-2/3') =", f1)
print ("Fraction('3, 4') =", f2)
print ("Fraction('.25') =", f3)

# Sum
print (f1, '+', f2, '=', f1 + f2)
print (f2, '+', f3, '=', f2 + f3)
```

    Fraction('-2/3') = -2/3
    Fraction('3, 4') = 3/4
    Fraction('.25') = 1/4
    -2/3 + 3/4 = 1/12
    3/4 + 1/4 = 1


Fractions can be initialized in several ways: as a *string*, as a pair of integers, or as a real number. The module also has a function called `gcd()` which calculates the greatest common divisor (gcd) of two integers.

### Operating System

Apart from the file system, the modules of the standard library also provides access to other services provided by the operating system.

Example:


```python
import os
import sys
import platform

def uid():
    """
    uid() -> returns the current user identification
    or None if not possible to identify
    """

    # Ambient variables for each operating system
    us = {'Windows': 'USERNAME',
        'Linux': 'USER'}

    u = us.get(platform.system())
    return os.environ.get(u)

print ('User:', uid())
print ('plataform:', platform.platform())
print ('Current dir:', os.path.abspath(os.curdir))
exep, exef = os.path.split(sys.executable)
print ('Executable:', exef)
print ('Executable dir:', exep)
```

    User: mayank
    plataform: Linux-4.15.3-gentoo-x86_64-Intel-R-_Core-TM-_i3-4000M_CPU_@_2.40GHz-with-gentoo-2.2.2
    Current dir: /home/mayank/code/mj/ebooks/python/lep/Section 1 - Core Python/Chapter 16 - Standard library
    Executable: python3.6
    Executable dir: /usr/bin


Process execution example:


```python
###################################
### TODO ##########################
###################################
###################################
###################################









# import sys
# from subprocess import Popen, PIPE

# # ping
# cmd = 'ping -c 1 '
# # No Windows
# if sys.platform == 'win32':
#     cmd = 'ping -n 1 '

# # Local just for testing
# host = '127.0.0.1'

# # Comunicates with another process
# # a pipe with the command stdout
# py = Popen(cmd + host, stdout=PIPE)

# # Shows command output
# print (py.stdout.read())
```

The *subprocess* module provides a generic way of running processes with Popen() function which allows communication with the process through operating system pipes.

Time
-----
Python has two modules to handle time:

+ *Time*: implements functions that allow using the time generated by the system.
+ *Datetime*: implements high-level types to perform date and time operations.

Example with time:


```python
import time

# localtime() Returns a date and local time in the form 
# of a structure called struct_time, which is a 
# collection with the items: year, month, day, hour, minute,
# secund, day of the week, day of the year and e daylight saving time
print (time.localtime())

# asctime() returns a date and hour with string, according to
# operating system configuration
print (time.asctime())

# time() returns system time in seconds
ts1 = time.time()

# gmtime() converts seconds to struct_time
tt1 = time.gmtime(ts1)
print (ts1, '->', tt1)

# Adding an hour
tt2 = time.gmtime(ts1 + 3600)

# mktime() converts struct_time  to seconds
ts2 = time.mktime(tt2)
print (ts2, '->', tt2)

# clock() returs time since the program started, in seconds
print ('The program took', time.clock(), \
    'seconds up to now...')

# Counting seconds...
for i in range(5):
    # sleep() waits the number of seconds specified as parameter
    time.sleep(1)
    print (i + 1, 'second(s)')
```

    time.struct_time(tm_year=2018, tm_mon=2, tm_mday=6, tm_hour=16, tm_min=32, tm_sec=33, tm_wday=1, tm_yday=37, tm_isdst=0)
    Tue Feb  6 16:32:33 2018
    1517914953.7361274 -> time.struct_time(tm_year=2018, tm_mon=2, tm_mday=6, tm_hour=11, tm_min=2, tm_sec=33, tm_wday=1, tm_yday=37, tm_isdst=0)
    1517898753.0 -> time.struct_time(tm_year=2018, tm_mon=2, tm_mday=6, tm_hour=12, tm_min=2, tm_sec=33, tm_wday=1, tm_yday=37, tm_isdst=0)
    The program took 5.131858836248371e-06 seconds up to now...
    1 second(s)
    2 second(s)
    3 second(s)
    4 second(s)
    5 second(s)



```python
import time
def countdown(x):
    # Counting seconds...
    for i in range(x):
        # sleep() waits the number of seconds specified as parameter
        time.sleep(1)
        print (i + 1, 'second(s)')

countdown(4)
print("countdown completed")
```

    1 second(s)
    2 second(s)
    3 second(s)
    4 second(s)
    countdown completed


In *datetime*, four types are defined for representing time:

+ *datetime*: date and time.
+ *date*: just date.
+ *time*: just time.
+ *timedelta*: time diference.

Example:


```python
import datetime

# datetime() receives as parameter:
# year, month, day, hour, minute, second and 
# returns an object of type datetime
dt = datetime.datetime(2020, 12, 31, 23, 59, 59)

# Objects date and time can be created from
# a datetime object
date = dt.date()
hour = dt.time()

# How many time to 12/31/2020
dd = dt - dt.today()

print ('Date:', date)
print ('Hour:', hour)
print ('How many time to 12/31/2020:', dd)

```

    Date: 2020-12-31
    Hour: 23:59:59
    How many time to 12/31/2020: 1060 days, 2:14:13.862233


### textwrap — Text wrapping and filling

The `textwrap` module provides some convenience functions, as well as `TextWrapper`, the class that does all the work. If you’re just wrapping or filling one or two text strings, the convenience functions should be good enough; otherwise, you should use an instance of `TextWrapper` for efficiency

* **textwrap.wrap** - Wraps the single paragraph in text (a string) so every line is at most width characters long. Returns a list of output lines, without final newlines. Optional keyword arguments correspond to the instance attributes of TextWrapper, documented below. width defaults to 70.


```python
import textwrap

def banner(txt):
    print("`)

txt = "    The   `textwrap` module provides some convenience functions, as well as `TextWrapper`, the class that does all the work. If you’re just wrapping or filling one or two text strings, the convenience functions should be good enough; otherwise, you should use an instance of `TextWrapper` for efficiency"
print(txt)


for t in textwrap.wrap(txt):
    print(t)
    
for t in textwrap.wrap(txt, width=100):
    print(t)
    
for t in textwrap.wrap(txt, width=60, initial_indent="* "):
    print(t)
```

        The   `textwrap` module provides some convenience functions, as well as `TextWrapper`, the class that does all the work. If you’re just wrapping or filling one or two text strings, the convenience functions should be good enough; otherwise, you should use an instance of `TextWrapper` for efficiency
        The   `textwrap` module provides some convenience functions, as
    well as `TextWrapper`, the class that does all the work. If you’re
    just wrapping or filling one or two text strings, the convenience
    functions should be good enough; otherwise, you should use an instance
    of `TextWrapper` for efficiency
        The   `textwrap` module provides some convenience functions, as well as `TextWrapper`, the class
    that does all the work. If you’re just wrapping or filling one or two text strings, the convenience
    functions should be good enough; otherwise, you should use an instance of `TextWrapper` for
    efficiency
    *     The   `textwrap` module provides some convenience
    functions, as well as `TextWrapper`, the class that does all
    the work. If you’re just wrapping or filling one or two text
    strings, the convenience functions should be good enough;
    otherwise, you should use an instance of `TextWrapper` for
    efficiency


* **textwrap.fill** - Wraps the single paragraph in text, and returns a single string containing the wrapped paragraph. fill() is shorthand for


```python
print(textwrap.fill(txt))
```

        The   `textwrap` module provides some convenience functions, as
    well as `TextWrapper`, the class that does all the work. If you’re
    just wrapping or filling one or two text strings, the convenience
    functions should be good enough; otherwise, you should use an instance
    of `TextWrapper` for efficiency


* **textwrap.shorten** - Collapse and truncate the given text to fit in the given width. First the whitespace in text is collapsed (all whitespace is replaced by single spaces). If the result fits in the width, it is returned. Otherwise, enough words are dropped from the end so that the remaining words plus the placeholder fit within width


```python
print(textwrap.shorten(txt, width=60))
print(textwrap.shorten(txt, width=50))
s = textwrap.shorten(txt, width=25, placeholder="...")
print(s)
print(len(s))
s = textwrap.shorten(txt, width=20, placeholder="...")
print(s)
print(len(s))
```

    The `textwrap` module provides some convenience [...]
    The `textwrap` module provides some [...]
    The `textwrap` module...
    24
    The `textwrap`...
    17


* **textwrap.dedent** - Remove any common leading whitespace from every line in text.


```python
textwrap.dedent(txt)
```




    'The   `textwrap` module provides some convenience functions, as well as `TextWrapper`, the class that does all the work. If you’re just wrapping or filling one or two text strings, the convenience functions should be good enough; otherwise, you should use an instance of `TextWrapper` for efficiency'



* **textwrap.indent** - Add prefix to the beginning of selected lines in text.


```python
print(textwrap.indent(txt.strip().replace(",", '\n'), '$ '))
```


    ---------------------------------------------------------------------------

    NameError                                 Traceback (most recent call last)

    <ipython-input-1-eb1b248f48a9> in <module>()
    ----> 1 print(textwrap.indent(txt.strip().replace(",", '\n'), '$ '))
    

    NameError: name 'textwrap' is not defined


### unicodedata

Get more details from unicode characters using `unicodedata` as shown in the example below


```python
import unicodedata

for i, c in enumerate(range(0x0958, 0x0968)):
    c = chr(c)
    print(i, '%04x' % ord(c), unicodedata.category(c), end=" : ")
    print(c, unicodedata.name(c))
```

    0 0958 Lo : क़ DEVANAGARI LETTER QA
    1 0959 Lo : ख़ DEVANAGARI LETTER KHHA
    2 095a Lo : ग़ DEVANAGARI LETTER GHHA
    3 095b Lo : ज़ DEVANAGARI LETTER ZA
    4 095c Lo : ड़ DEVANAGARI LETTER DDDHA
    5 095d Lo : ढ़ DEVANAGARI LETTER RHA
    6 095e Lo : फ़ DEVANAGARI LETTER FA
    7 095f Lo : य़ DEVANAGARI LETTER YYA
    8 0960 Lo : ॠ DEVANAGARI LETTER VOCALIC RR
    9 0961 Lo : ॡ DEVANAGARI LETTER VOCALIC LL
    10 0962 Mn : ॢ DEVANAGARI VOWEL SIGN VOCALIC L
    11 0963 Mn : ॣ DEVANAGARI VOWEL SIGN VOCALIC LL
    12 0964 Po : । DEVANAGARI DANDA
    13 0965 Po : ॥ DEVANAGARI DOUBLE DANDA
    14 0966 Nd : ० DEVANAGARI DIGIT ZERO
    15 0967 Nd : १ DEVANAGARI DIGIT ONE


### ctypes

Below code allows to get the value stored in memory using ID of the variable.


```python
import ctypes
a = "G.V. is a good person"
print(ctypes.cast(id(a), ctypes.py_object).value)
```

    G.V. is a good person


## Excersise - Files I/O


1. Write a Python program to read an entire text file.     

2. Write a Python program to read first n lines of a file.     

3. Write a Python program to append text to a file and display the text.     

4. Write a Python program to read last n lines of a file.     

5. Write a Python program to read a file line by line and store it into a list.     

6. Write a Python program to read a file line by line store it into a variable.     

7. Write a Python program to read a file line by line store it into an array.     

8. Write a python program to find the longest words.     

9. Write a Python program to count the number of lines in a text file.     

10. Write a Python program to count the frequency of words in a file.     

11. Write a Python program to get the file size of a plain file.     

12. Write a Python program to write a list to a file.     

13. Write a Python program to copy the contents of a file to another file .     

14. Write a Python program to combine each line from first file with the corresponding line in second file.     

15. Write a Python program to read a random line from a file.     

16. Write a Python program to assess if a file is closed or not.     

17. Write a Python program to remove newline characters from a file.     

## Excersise - OS, zip, csv

* Write a program to list all the files in the given directory along with their length and last modification time. The output should contain one line for each file containing filename, length and modification date separated by tabs.
* Write a program to print directory tree. The program should take path of a directory as argument and print all the files in it recursively as a tree.
* Write a python program zip.py to create a zip file. The program should take name of zip file as first argument and files to add as rest of the arguments.
* Create a csv file and traverse it using `csv` module

## Excersise - textwrap

**Pre**: Store entire text of "Excersise - OS, zip, csv" in a string (exc)

1. Print the `exc` with one line spacing
2. Print summary of the `exc` so that only 50 chars are shown
3. Print `exc` in such a manner that it replaces starting "*" with 'Question auto_number:'.


## Excersise - Time

* Write a fuction which can find the time of execution duration of each excercise in sec & also in min. 
* Write a clock which can return the time in multiple timezones.
