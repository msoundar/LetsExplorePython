
### String Operations

String data type provide multiple `methods`/`functions` for performing various operations. 

Lets find, all the string attributes using `dir` function.


```python
print(dir("animal"))
```

    ['__add__', '__class__', '__contains__', '__delattr__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__getitem__', '__getnewargs__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__iter__', '__le__', '__len__', '__lt__', '__mod__', '__mul__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__rmod__', '__rmul__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', 'capitalize', 'casefold', 'center', 'count', 'encode', 'endswith', 'expandtabs', 'find', 'format', 'format_map', 'index', 'isalnum', 'isalpha', 'isdecimal', 'isdigit', 'isidentifier', 'islower', 'isnumeric', 'isprintable', 'isspace', 'istitle', 'isupper', 'join', 'ljust', 'lower', 'lstrip', 'maketrans', 'partition', 'replace', 'rfind', 'rindex', 'rjust', 'rpartition', 'rsplit', 'rstrip', 'split', 'splitlines', 'startswith', 'strip', 'swapcase', 'title', 'translate', 'upper', 'zfill']


Since it provide so many attributes, we are only going to cover few of the important ones in this section

#### Creation / Assignation

We can create new string using assignation operator.


```python
name = 'Camel'
print(name, id(name))

name = "Roshan\tMusheer"
print(name, id(name))
```

    Camel 89006296
    Roshan	Musheer 88962224


In the above example, we have created two new strings with data `Camel` and `Roshan\tMusheer`. 

#### Single / Double quote in Double / Single  quote string

We might have a situation, were we need to have single quote or double quote character with-in the string. The easiest way to achieve it by using scape character `\`.


```python
a = 'Roshan\tMusheer\'s car'
print(a)
```

    Roshan	Musheer's car



```python
a = 'Roshan Musheer said: "Good Work"'
print(a)
```

    Roshan Musheer said: "Good Work"



```python
animal = "Camel 'Are good in deserts.'"
```

We can also use something like following, were we have used different quote to denote the string.


```python
animal = 'Camel "Are good in deserts."'
print(animal, id(animal))
a = "Roshan\tMusheer's car is always too small for him. ;)"
print(a)
```

    Camel "Are good in deserts." 88924032
    Roshan	Musheer's car is always too small for him. ;)


#### Concatenation

String concatenation is a process of joining two or more strings into a **new single string**. As we have already discussed that string is an immutable datatype thus we have to create a new string for concatenation, what that means is the original strings will still remain the same and new one will be created using the texts from the originals. 

There are multiple ways in which we can achive the concatenation. The most common method of achiving the concatenation, is to use `+` operator. 

Lets take an example, where we have three string's and lets try to concatenate them using it.


```python
animal = "lion"
space = " "
st_the = "The"
st_action = "ran away !!!"
st = st_the + space + animal + space + st_action
```


```python
print(st, id(st))
print(animal, id(animal))
print(st_the, id(st_the))
print(st_action, id(st_action))
```

    The lion ran away !!! 89055928
    lion 89006632
    The 89009096
    ran away !!! 88964336


### Interpolation

string interpolation (or variable interpolation, variable substitution, or variable expansion) is the process of evaluating a string literal containing one or more placeholders, yielding a result in which the placeholders are replaced with their corresponding values.


```python
s = "Ja, Ich bin ein Mann"
print('Size of `%s` => %d' % (s, len(s)))
print('Size of `%s` => %d' % (s, s.__len__()))
```

    Size of `Ja, Ich bin ein Mann` => 20
    Size of `Ja, Ich bin ein Mann` => 20



```python
def size(strdata):
    c = 0
    for a in strdata:
        c+=1
    return c

print(size("Anshu"))
```

    5


The operator `%` is used for string interpolation. The interpolation is more efficient in use of memory than the conventional concatenation.

Symbols used in the interpolation:

+ %s: *string*.
+ %d: integer.
+ %o: octal.
+ %x: hexacimal.
+ %f: real.
+ %e: real exponential.
+ %%: percent sign.

Symbols can be used to display numbers in various formats.

*Example*:


```python
# Zeros left
print ('Days in years are %02d, dummy value %02d.' % (356, 1))

# Real (The number after the decimal point specifies how many decimal digits )
print ('Percent: %.1f%%, Exponencial:%.2e' % (5.333, 0.00000031403030))

# Octal and hexadecimal
print ('Decimal: %d, Octal: %o, Hexadecimal: %x' % (10, 10, 10))
```

    Days in years are 356, dummy value 01.
    Percent: 5.3%, Exponencial:3.14e-07
    Decimal: 10, Octal: 12, Hexadecimal: a



```python
# Zeros left
print ('Now is %02d:%02d.' % (6, 30))

# Real (The number after the decimal point specifies how many decimal digits )
print ('Percent: %.1f%%, Exponencial:%.2e' % (5.333, 0.00314))

# Octal and hexadecimal
print ('Decimal: %d, Octal: %o, Hexadecimal: %x' % (10, 10, 10))
```

    Now is 06:30.
    Percent: 5.3%, Exponencial:3.14e-03
    Decimal: 10, Octal: 12, Hexadecimal: a


### `format`

In addition to interpolation operator `%`, the string method and function `format()` is available.

> The function `format()` can be used only to format one piece of data each time.

*Examples*:


```python
# Parameters are identified by order
msg = '{} was {} of {}'

print(msg.format('Mayank', 'reportee', 'Roshan Musheer'))
```

    Mayank was reportee of Roshan Musheer


If you have more numbers of "{}" than you have variables then you will get following error.


```python
try:
    msg = '{} was {} of {}. Testing {}'
    print(msg.format('Mayank', 'reportee', 'Roshan Musheer'))
except Exception as e:
    print(e)
```

    tuple index out of range



```python
# using extra `{}` as escape characters 
try:
    msg = '{} was {} of {}. Testing {{}}'
    print(msg.format('Mayank', 'reportee', 'Roshan Musheer'))
except Exception as e:
    print(e)
```

    Mayank was reportee of Roshan Musheer. Testing {}


but opposite is not true


```python
try:
    msg = '{} was {}'
    print(msg.format('Mayank', 'reportee', 'Roshan Musheer'))
except Exception as e:
    print(e)
```

    Mayank was reportee



```python
# Parameters are identified by order

msg = '{0} was {2} of {1}'

print(msg.format('Mayank', 'Mr. K.V. Pauly', 'reportee'))
```

    Mayank was reportee of Mr. K.V. Pauly



```python
# Parameters are identified by name

msg = '{greeting}, it is {hour:02d}:{minute:02d} AM.'

print(msg.format(greeting='Good Morning', minute=2, hour=10))
print(msg)
```

    Good Morning, it is 10:02 AM.
    {greeting}, it is {hour:02d}:{minute:02d} AM.


it treats `msg` string as a template and generates a new string everytime we use `format` function.


```python
# Builtin function format()
print ('Pi =', format(3.14159, '.3e'))
print ('Pi =', format(3.14159, '.1e'))
```

    Pi = 3.142e+00
    Pi = 3.1e+00


#### Formatting in details

`format` provides many options which can be used to process the data interpolation 

##### Basic formatting 


```python
'{} {}'.format('सूर्य', 'नमस्कार')
```




    'सूर्य नमस्कार'




```python
'{1} {0}'.format('सूर्य', 'नमस्कार')
```




    'नमस्कार सूर्य'




```python
'{sun} {hello}'.format(sun='सूर्य', hello='नमस्कार')
```




    'सूर्य नमस्कार'



##### Padding

We can add padding or align the text using it. 


```python
'{hello_sun:20}'.format(hello_sun='सूर्य नमस्कार')
```




    'सूर्य नमस्कार       '




```python
'{:4}'.format('Bonjour')
```




    'Bonjour'




```python
'{:.5}'.format('Bonjour')
```




    'Bonjo'




```python
# mix and match example

hello = '{:10.4}'.format('Bonjour')
print(hello, "<")
print(len(hello))
```

    Bonj       <
    10


##### Right Align


```python
s = '{:>30}'.format('सूर्य नमस्कार')
print(">", s, "< end")
print(len(s))
```

    >                  सूर्य नमस्कार < end
    30



```python
s = '{:>30}'.format(10)
print(s)
s = '{:>30}'.format(20)
print(s)
s = '{:>30}'.format(-30)
print(s)
print("*"*30)
print('Total {:>24}'.format(0))
```

                                10
                                20
                               -30
    ******************************
    Total                        0


If the length of string is more than specified length than it print entire string


```python
s = '{:>2}'.format('सूर्य नमस्कार')
print(s, "< end")
print(len(s))
```

    सूर्य नमस्कार < end
    13


##### Left Align


```python
s = '{:<30}'.format('सूर्य नमस्कार')
print(s, "< end")
print(len(s))
```

    सूर्य नमस्कार                  < end
    30


If the length of string is more than specified length than it print entire string


```python
s = '{:<2}'.format('सूर्य नमस्कार')
print(s)
print(len(s))
```

    सूर्य नमस्कार
    13


If you wish to add padding with custom character then it can be done using the following method


```python
s = '{:_<30}'.format('सूर्य नमस्कार')
print(s)
```

    सूर्य नमस्कार_________________



```python
s = '{:*<30}'.format('Pay Rs100')
print(s)
```

    Pay Rs100*********************


##### Center Align


```python
'{:^20}'.format('Bonjour')
```




    '      Bonjour       '




```python
'{:^7}'.format('こんにちは')
```




    ' こんにちは '



We can have even programmatically define the alignment as shown in below example.


```python
lst = ["<", ">", "^"]

for align in lst:
    print('>{:{align}{width}}<'.format('Bonjour', align=align, width='20'))
```

    >Bonjour             <
    >             Bonjour<
    >      Bonjour       <


##### Truncate

Truncate allows to trim long string to specified length, the syntax is as follows

```python
{<string length>.<text length>}
```

- String_length: It is the final string length
- text_length: It is the length of truncated text which will be present in final string


```python
s = '>{:40.4}<'.format('testdd नमस्कार')
print(len(s))
print(s)
```

    42
    >test                                    <


In the above example, although the string size is 101 only 4 characters are present in the final string.


```python
'{:^10.7}'.format('Ich bin ein Mann')
```




    ' Ich bin  '



another example, with different data types


```python
'{:.{prec}}, {:.{prec}f}'.format('Ich bin ein Mann', 2.22, prec=5)
```




    'Ich b, 2.22000'



#### Numbers 

**Decimals**


```python
'{:d}'.format(1980)
```




    '1980'




```python
'{:5d}'.format(119)
```




    '  119'




```python
'{:5d}'.format(1111119)
```




    '1111119'




```python
'{:05d}'.format(19)
```




    '00019'



**Float**


```python
'{:f}'.format(3.141592653589793)
```




    '3.141593'




```python
'{:2f}'.format(3.141592653589793)
```




    '3.141593'




```python
'{:04f}'.format(11.9)
```




    '11.900000'




```python
# both side padding in float number

'{:08.04f}'.format(3.1)
```




    '003.1000'




```python
### Need to find for complex & boolean numbers
## '{:+d+d}'.format(-3 + 2j)
```

##### Signs on numbers

We can add `+` and `-` signs before the numbers as shown in the examples below


```python
'{:+5d}'.format(11)
```




    '  +11'




```python
'{:+5d}'.format(119291)
```




    '+119291'




```python
'{:5d}'.format(+11)

```




    '   11'




```python
'{:5d}'.format(-11)
```




    '  -11'




```python
'{:+5d}'.format(-11)
```




    '  -11'




```python
'{:-5d}'.format(11)
```




    '   11'



##### Dictionary 


```python
user = {'name': 'Mayank', 'surname': 'Johri'}
print(user)
'{u[name]} {u[surname]}'.format(u=user)
```

    {'name': 'Mayank', 'surname': 'Johri'}





    'Mayank Johri'



lets try similar with a list of dictionaries


```python
users = [
    {'name': 'Mayank', 'surname': 'Johri'},
    {'name': 'Roshan', 'surname': 'Musheer'},
    {'name': 'Mohan', 'surname': 'Shah'},
    {'name': 'Sachin', 'surname': 'Shah'},
    {'name': 'Rajeev', 'surname': 'Jain'}
]

for user in users:
    print('{u[name]} {u[surname]}'.format(u=user))
```

    Mayank Johri
    Roshan Musheer
    Mohan Shah
    Sachin Shah
    Rajeev Jain


##### List 

List items can also be selected as shown below


```python
lst = list(range(10))
'{l[2]} {l[7]}'.format(l=lst)
```




    '2 7'



##### Date & Time 


```python
from datetime import datetime
'{:%Y-%m-%d %H:%M}'.format(datetime(2017, 12, 23, 14, 15))
```




    '2017-12-23 14:15'



##### Class


```python
class Yoga(object):

    def __repr__(self):
        return 'सूर्य नमस्कार'
    
    
'{0!r} <-> {0!a}'.format(Yoga())
```




    'सूर्य नमस्कार <-> \\u0938\\u0942\\u0930\\u094d\\u092f \\u0928\\u092e\\u0938\\u094d\\u0915\\u093e\\u0930'



#### Literal String / Formatted string

It is the new Interpolation method as it is implemented in `Python 3.6`. 


```python
name = 'World'
program = 'Python'

hw = f'Hello {name:10}! This is {program}'
print(hw)
print(id(hw))

name = 'G.V.'
program = 'Python'
# will not update the hw string
print(name)
print(hw)
hw = F'Hello {name}! This is {program}'
print(hw)
print(id(hw))
```

    Hello World     ! This is Python
    87170984
    G.V.
    Hello World     ! This is Python
    Hello G.V.! This is Python
    87264192



```python
name = 'Ravi'
program = 'Ruby'
hw1 = hw
print(hw1)
```

    Hello G.V.! This is Python


#### `startswith` & `endswith`


```python
username = "Murthy "
# Strings are objects
print(username.startswith('Mu'))
```

    True



```python
# This will check if any of the keywork listed in designation is 
# present in the username
designation = ("Dr.", "Mr.")

print(username.startswith(designation))
```

    False



```python
username = "Mr. K.V Pauly"
designation = ("Dr.", "Mr.")

print(username.startswith(designation))
```

    True



```python
print(username.endswith('thy'))
print(username.endswith('ly'))
```

    False
    True


#### `strip()`, `lstrip()` and `rstrip()`

This function removes the spaces and few other special characters from both side of the printable characters from the string.


```python
s = "   \n\tMurthy\tSwamy\n  "

print("s >", s, "<", len(s))
print("*"*29)
d = s.strip()
print("d >", d, "<", len(d))
print("*"*29)
# Original String still remains the same.
print("s >", s, "<", len(s))
```

    s >    
    	Murthy	Swamy
       < 20
    *****************************
    d > Murthy	Swamy < 12
    *****************************
    s >    
    	Murthy	Swamy
       < 20



```python
st = "    \n\tMayank\tJohri    "

print(">", st.rstrip(), "<")
print(">", st.lstrip(), "<")
```

    >     
    	Mayank	Johri <
    > Mayank	Johri     <


#### `join()`

It allows to join elements of second using the first or in other words, first is used to join the elements of second and a string is created. 


```python
m = "Mohan Shah"
x = ["mon", "tues", "wed"]
y = ","
a = ["On Leave"]

print(y.join(x)) # -> mon,tues,wed
print(m.join(y)) 
print(y.join(a))
```

    mon,tues,wed
    ,
    On Leave


> Create a string from a list of string items


```python
" ".join(x)
```




    'mon tues wed'




```python
", ".join(x)
```




    'mon, tues, wed'




```python
book_desc = ["This", "book", "is good"]
" ".join(book_desc)
```




    'This book is good'




```python
# str function do not work :(
str(book_desc)
```




    "['This', 'book', 'is good']"



Your list should only have string elements, else you will get error message as follows


```python
try:
    book_desc = ["This", "book", "is good", 1010]
    txt = ", ".join(book_desc)
except Exception as e:
    print("error:".upper(), e)
```

    ERROR: sequence item 3: expected str instance, int found


#### `capitalize`, `center`


```python
myStr = "maya Deploy, version: 0.0.3 "

print(myStr.capitalize())
```

    Maya deploy, version: 0.0.3 



```python
myStr = " maya Deploy, version: 0.0.3 "

print(myStr.capitalize())
```

     maya deploy, version: 0.0.3 



```python
# Unicode characters are also handled.

my_str = "ß Testing"
print(my_str.capitalize())
```

    SS testing



```python
print(myStr.center(60))
print(myStr.center(60, "-"))
print(myStr.center(60, "*"))
```

                    maya Deploy, version: 0.0.3                 
    --------------- maya Deploy, version: 0.0.3 ----------------
    *************** maya Deploy, version: 0.0.3 ****************



```python
try:
    print(myStr.center(60, "*~"))
except Exception as e:
    print("ERROR", e)
```

    ERROR The fill character must be exactly one character long



```python
# My string is larger than the number, nothing will happen

print(myStr.center(12, "*"))
```

     maya Deploy, version: 0.0.3 



```python
anitha="anitha Is Good"
print(">", anitha.capitalize().center(40), "<")
```

    >              Anitha is good              <


#### `count`, `find`

`count` returns the number of time the substring is presend in the give string. It do not find the substring using recursion.


```python
print(myStr.count('a'))
print(myStr.count('0.'))
print(myStr.count('20.'))
```

    2
    2
    0



```python
# no recursion, once searched it will not go back to 
# search for other permutations on the searched substring 
# for substring under search. 

my_txt = "dadadada"
print(my_txt.count("dada"))
```

    2



```python
# Find returns the index of first occurence of the 
# substring in the string else returns -1 (False)

print(myStr.find("g"))
print(myStr.find("e"))
my_txt = "dadadada"
print(my_txt.find("ada"))
```

    -1
    7
    1


> **Note**: The find() method should be used only if you need to know the position of sub. To check if sub is a substring or not, use the `in` operator:

checking: substring in main_string : returns true or false


```python
print("ma" in myStr)
```

    True



```python
print("M" in myStr)
```

    False


### `str` in-build module

Strings implement all of the common sequence operations, along with the additional methods described below.


```python
c = "one"
print(c.isalpha())
c = "onetwo"
print(c.isalpha())
c = "twelvethousandthreehundredsix"
print(c.isalpha())
c = "twelvethousandthreehundredandsix"
print(c.isalpha())
```

    True
    True
    True
    True



```python
# Anything other than alpha will return false as shown below

c = "1"
print(c.isalpha())
c = "one two"
print(c.isalpha())
c = "twelve thousand three hundred and six"
print(c.isalpha())

```

    False
    False
    False



```python
# Solving the space issue.
c = "twelve thousand three hundred and six"
print(c.replace(" ", "").isalpha())
```

    True



```python
superscripts = "\u00B2"
five = "\u0A6B"
five_punjabi = "੫"
ten_hindi = "१०"
num_one = "1"
one = "one"
fractions = "\u00BC"
```


```python
print(superscripts)
print(five)
print(five_punjabi)
print(ten_hindi)
print(num_one)
print(one)
print(fractions)
```

    ²
    ੫
    ੫
    १०
    1
    one
    ¼


#### `isdecimal()`


```python
print(superscripts, "\t", superscripts.isdecimal())
print(five, "\t", five.isdecimal())
print(five_punjabi, "\t", five_punjabi.isdecimal())
print(ten_hindi, "\t", ten_hindi.isdecimal())
print(num_one, "\t", num_one.isdecimal())
print(one, "\t", one.isdecimal())
print(fractions, "\t", fractions.isdecimal())
```

    ² 	 False
    ੫ 	 True
    ੫ 	 True
    १० 	 True
    1 	 True
    one 	 False
    ¼ 	 False



```python
print("10 ->", "10".isdecimal())
print("10.001 ->","10.001".isdecimal())
```

    10 -> True
    10.001 -> False



```python
# Any text other than numbers will result in False
str = u"this 2009";  
print(str.isdecimal())
```

    False


#### `isdigit`


```python
# str.isdigit() (Decimals, Subscripts, Superscripts)
print(superscripts, "\t", superscripts.isdigit())
print(five, "\t" , five.isdigit())
print(five_punjabi, "\t" , five_punjabi.isdigit())
print(ten_hindi, "\t" , ten_hindi.isdigit())
print(num_one, "\t" , num_one.isdigit())
print(one, "\t" , one.isdigit())
print(fractions, "\t" , fractions.isdigit())
```

    ² 	 True
    ੫ 	 True
    ੫ 	 True
    १० 	 True
    1 	 True
    one 	 False
    ¼ 	 False



```python
print("10".isdigit())
str = u"this 2009";  
print(str.isdigit())

str = u"23443.434";
print(str.isdigit())
```

    True
    False
    False


#### `str.isnumeric`

- Digits, 
- Fractions, 
- Subscripts, 
- Superscripts


```python
print(superscripts, "\t", superscripts.isnumeric())
print(five, "\t", five.isnumeric())
print(five_punjabi, "\t", five_punjabi.isnumeric())
print(ten_hindi, "\t", ten_hindi.isnumeric())
print(num_one, "\t", num_one.isnumeric())
print(one, "\t", one.isnumeric())
print(fractions, "\t", fractions.isnumeric())
```

    ² 	 True
    ੫ 	 True
    ੫ 	 True
    १० 	 True
    1 	 True
    one 	 False
    ¼ 	 True


#### `isalnum`

Any kind of number: be it alpha or numeric


```python
print(superscripts, "\t", superscripts.isalnum())
print(five, "\t", five.isalnum())
print(five_punjabi, "\t", five_punjabi.isalnum())
print(ten_hindi, "\t", ten_hindi.isalnum())
print(num_one, "\t", num_one.isalnum())
print(one, "\t", one.isalnum())
print(fractions, "\t", fractions.isalnum())
```

    ² 	 True
    ੫ 	 True
    ੫ 	 True
    १० 	 True
    1 	 True
    one 	 True
    ¼ 	 True



```python
print("one".isalnum())
print("thirteen".isalnum())
tenOne = "10One"
print(tenOne.isalnum())
ten_One = "10 One"
print(ten_One.isalnum())
```

    True
    True
    True
    False


### Reverse String

Creates a new list with elements in reverse order

- **Method 1**


```python
s = "जरूस ाक हबुस हबुस"
print(s[::-1])
```

    सुबह सुबह का सूरज


- **Method 2**


```python
s = "जरूस ाक हबुस हबुस"
print(''.join(reversed(s)))
s = "nuS gninroM"
print(''.join(reversed(s)))
```

    सुबह सुबह का सूरज
    Morning Sun



```python
# inner working of method 2
print(reversed(s))
print(tuple(reversed(s)))
print("".join(reversed(s)))
```

    <reversed object at 0x00000000055390B8>
    ('M', 'o', 'r', 'n', 'i', 'n', 'g', ' ', 'S', 'u', 'n')
    Morning Sun


### case-insensitive string comparison

##### for ASCII strings


```python
string1 = 'Hello'
string2 = 'helLo'

if string1.lower() == string2.lower():
    print("The strings are the same (case insensitive)")
else:
    print("The strings are not the same (case insensitive)")
```

    The strings are the same (case insensitive)


##### for unicode strings


```python
str_lower = "Σίσυφος"
str_upper = "ΣΊΣΥΦΟΣ"

if str_upper.lower() == str_lower.lower():
    print("The strings are the same (case insensitive)")
else:
    print("The strings are not the same (case insensitive)")
```

    The strings are the same (case insensitive)


but fails in some cases


```python
str_lower = "ß"
str_upper = "SS"

if str_upper.lower() == str_lower.lower():
    print("The strings are the same (case insensitive)")
else:
    print("The strings are not the same (case insensitive)")
```

    The strings are not the same (case insensitive)


So the best bet is using `casefold`. Lets replace `lower` to `casefold` in the above example 


```python
str_lower = "ß"
str_upper = "SS"

if str_upper.casefold() == str_lower.casefold():
    print("The strings are the same (case insensitive)")
else:
    print("The strings are not the same (case insensitive)")
```

    The strings are the same (case insensitive)



```python
# s = u'הוא אוסף אתכם מחר בשלוש וחצי.'
# len(s) #29
# byt = bytes(s, encoding="utf8")
# len(byt)
# print(byt)
```

### Interning Strings

It is a method of storing only one copy of each distinct string value, which must be immutable. Interning strings makes some string processing tasks more time- or space-efficient at the cost of requiring more time when the string is created or interned.


```python
s1 = "Mayank"
s2 = "Mayank"

print(s1, id(s1))
print(s2, id(s2))
print(s1 is s2)
```

    Mayank 89284424
    Mayank 89284424
    True



```python
s1 = "Mayank_Johri"
s2 = "Mayank_Johri"

print(s1, id(s1))
print(s2, id(s2))
print(s1 is s2)
```

    Mayank_Johri 89316848
    Mayank_Johri 89316848
    True



```python
# Special Case where they are treated as different.
s1 = "a"*21
s2 = "aaaaaaaaaaaaaaaaaaaaa"

print(s1, id(s1))
print(s2, id(s2))
print(s1 is s2)
print(s1 == s2)
```

    aaaaaaaaaaaaaaaaaaaaa 89323680
    aaaaaaaaaaaaaaaaaaaaa 89291632
    False
    True



```python
s1 = "a"*100
s2 = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"

print(s1, id(s1))
print(s2, id(s2))
print(s1 is s2) # Same 
print(s1 == s2) # Equal
```

    aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa 89350344
    aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa 89350800
    False
    True



```python
s1 = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
s2 = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"

print(s1, id(s1))
print(s2, id(s2))
print(s1 is s2)
print(s1 == s2)
```

    aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa 89350800
    aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa 89350800
    True
    True


By default only strings without spaces are stored in intern memory 


```python
s1 = "M "
s2 = "M "

print(s1, id(s1))
print(s2, id(s2))
print(s1 is s2)
print(s1 == s2)
```

    M  89439344
    M  89437664
    False
    True



```python
s1 = "M"
s2 = "M"

print(s1, id(s1))
print(s2, id(s2))
print(s1 is s2)
print(s1 == s2)
```

    M 33397368
    M 33397368
    True
    True


NOTE: Special Chac's matter and not the size

Forcefully using same instance


```python
from sys import intern

s1 = intern("Mayank  Johri")
s2 = intern("Mayank_Johri")
s4 = intern("Mayank  Johri")

print(s1, id(s1))
print(s2, id(s2))
print(s4, id(s4))
```

    Mayank  Johri 89317296
    Mayank_Johri 89435696
    Mayank  Johri 89317296



```python
j = intern("Mayank johri")
j1 = intern("Mayank johri")
print(j, id(j))
print(j1, id(j1))
print(s1, id(s1))
```

    Mayank johri 89434096
    Mayank johri 89434096
    Mayank Johri 82198896


### References

- https://www.python.org/dev/peps/pep-3101/
- https://en.wikipedia.org/wiki/String_interning
- https://en.wikipedia.org/wiki/Escape_character
- https://en.wikipedia.org/wiki/Escape_sequences_in_C
