
## Numeric Data Types

Python provide following *builtins* numeric data types:

+ Integer (`int`)
+ Floating Point real (`float`)
+ Complex (`complex`)
+ Boolean (`True`, `False`, `None` (maybe))

### Integer

Just as the name suggests, Integer consists of both `+ve` and `-ve` integers with no upper or lower size limit. Also they are immutable data types.

They can be written in any of the following format


```python
nom = 100
long_int_2 = 10011111988
print(long_int_2, type(long_int_2))
```

    10011111988 <class 'int'>


With Python version 3.x, to increase the readability of large numbers, it allows them to be written in the following format,


```python
long_int = 999_100_000_009_292    # with 3.x
print(long_int, type(long_int))
```

    999100000009292 <class 'int'>


> **<center>Note</center>**
> <hr/>
> Since, Python 2.4 <b>`long`</b> has been merged in <b>`int`</b>. (PEP 237)


```python
i = 99999999999999999999999999999999999999999999999999999999999999999999999999999
print(i)
print(type(i))
```

    99999999999999999999999999999999999999999999999999999999999999999999999999999
    <class 'int'>



```python
i = -9999999999999999999999999999999999999999999999999999999999999999999999999999999
print(i)
print(type(i))
```

    -9999999999999999999999999999999999999999999999999999999999999999999999999999999
    <class 'int'>


The builtin function `int()` can be used to convert other types to integer, including base changes. 

*Example*:


```python
# Converting real to integer
print ('int(3.14) =', int(3.14))
print ('int(3.64) =', int(3.64))    # Not rounding the values
print('int("22") =', int("22"))
```

    int(3.14) = 3
    int(3.64) = 3
    int("22") = 22


Note: Converting other data types to `int` such as `float` within string will fall as shown in the below example
```PYTHON
print('int("22.0") !=', int("22.0"))
```

```log
---------------------------------------------------------------------------
ValueError                                Traceback (most recent call last)
<ipython-input-9-8e518d0771bb> in <module>()
----> 1 print('int("22.0") !=', int("22.0"))

ValueError: invalid literal for int() with base 10: '22.0'
```     

In order to covert the above string, it should be first converted into its actual format (in this case `float`) and than converted to `int` as shown in the belwo example


```python
print('int("22.0") ==', int(float("22.8")))
```

    int("22.0") == 22


Also, complex numbers can't be converted to integers as shown below, instead we can use `complex.real` to achieve it.

```python
print("int(3+4j) =", int(3 + 4j))
```
```log
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
<ipython-input-11-6fb3672eabe6> in <module>()
----> 1 print("int(3+4j) =", int(3 + 4j))

TypeError: can't convert complex to int
```



```python
# Getting the integer from complex number

a = 3.422 + 4j
i = int(a.real)
print(i)
```

    3



```python
# Converting integer to real
print ('float(5) =', float(5))
```

    ('float(5) =', 5.0)



```python
print('float("22.8") ==', float("22.8"))
```

    ('float("22.8") ==', 22.8)



```python
# Auto conversion

# Calculation between integer and real results in real
print ('5 / 2 + 3 = ', 5 / 2 + 3)
```

    5 / 2 + 3 =  5.5



```python
x = 3.5
y = 2.5
z = x + y
print(x, y, z)
print(type(x), type(y), type(z))
z = int(z)
print(x, y, z)
print(type(x), y, type(z))
```

    3.5 2.5 6.0
    <class 'float'> <class 'float'> <class 'float'>
    3.5 2.5 6
    <class 'float'> 2.5 <class 'int'>



```python
# Integers in other base
print ("int('20', 8) =", int('20', 8)) # base 8
print ("int('20', 16) =", int('20', 16)) # base 16
```

    int('20', 8) = 16
    int('20', 16) = 32



```python
# Operations with complex numbers
c = 3 + 4j
print ('c =', c)

print ('Real Part:', c.real)
print ('Imaginary Part:', c.imag)
print ('Conjugate:', c.conjugate())
```

    c = (3+4j)
    Real Part: 3.0
    Imaginary Part: 4.0
    Conjugate: (3-4j)


### Converting Unicode numbers to numbers

#### Integers

We can use `unicodedata`'s `numeric` function to convert single unicode **digit** to integer 


```python
from unicodedata import numeric

num = "੪"   # 4 in punjabi
print(numeric(num))
```

    4.0



```python
from unicodedata import numeric
num = "൰"   # 10 in Malayalam
print(numeric(num))
```

    10.0


#### Float


```python
from unicodedata import numeric

threebyfour = "¾"
print(numeric(threebyfour))
```

    0.75


`float` function can also convert few of the unicode numeric data type but not all as shown in the below examples.


```python
print(float('۵۵.۵'))
```

    55.5



```python
try:
    print(float('੪੪.൫൯൪'))
except Exception as e:
    print(e)
```

    44.594


but might fail for special unicode numbers such as `൰`, '൱' etc which represent `10` and `100` respectively.


```python
try:
    print(float('൰'))
except Exception as e:
    print("!!! ERROR !!!:",e)
```

    !!! ERROR !!!: could not convert string to float: '൰'

