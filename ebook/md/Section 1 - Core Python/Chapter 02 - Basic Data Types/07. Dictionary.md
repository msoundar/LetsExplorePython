
## Dictionary

A dictionary is a list of associations composed by a unique key and corresponding structures. Dictionaries are mutable, like lists.

The key must be an immutable type, usually strings, but can also be tuples or numeric types. On the other hand the items of dictionaries can be either mutable or immutable. The Python dictionary provides no guarantee that the keys are ordered.

Syntax:
```python
dictionary = {'a': a, 'b': b, ..., 'z': z}
```
Structure:

![Structure of a dictionary](files/Dictionary.png)

Example of a dictionary:

```python
brands = {'name': 'Dabar', 'band': 'Honey'}
```
Acessing elements:

```python
print (brands['name'])
```
Adding/Updating elements:
```python
brands['key_1'] = '120'
```
Removing one elemento from a dictionary:
```python
del brands['key_1']
```

Getting the items, keys and values:
```python
items = brands.items()
keys = brands.keys()
values = brands.values()
```

**Examples with dictionaries**:


```python
brands = {'name': 'Dabar', 'band': 'Honey'}

items = brands.items()
keys = brands.keys()
values = brands.values()
print(items)
print(keys)
print(values)
```

    dict_items([('name', 'Dabar'), ('band', 'Honey')])
    dict_keys(['name', 'band'])
    dict_values(['Dabar', 'Honey'])


### Multiple values with same key 


```python
dic = {'name': 'Dabar', 'name': 'Dabar New', 'band': 'Honey'}
print(dic)
print(len(dic))
```

    {'name': 'Dabar New', 'band': 'Honey'}
    2


> **NOTE**: Last key will override the previous one


```python
for i, j in {"a": "test", "b": "test2"}.items():
    print(i, j)
```

    a test
    b test2



```python

progs = {
    "India": "Chennai",
    "South Africa": "Sant Petersberg",
    "England": "London"
}

# More progs
progs['Bangaladesh'] = "Dhaka"

print(progs)
```

    {'India': 'Chennai', 'South Africa': 'Sant Petersberg', 'England': 'London', 'Bangaladesh': 'Dhaka'}


### Delete a key/value pair


```python
##### Why its failing
progs = {'India': 'Chennai', 'South Africa': 'Sant Petersberg', 'England': 'London', 'Bangaladesh': 'Dhaka'}
print(progs)
del progs["England"]
print(progs)
```

    {'India': 'Chennai', 'South Africa': 'Sant Petersberg', 'England': 'London', 'Bangaladesh': 'Dhaka'}
    {'India': 'Chennai', 'South Africa': 'Sant Petersberg', 'Bangaladesh': 'Dhaka'}


### Nested Dictionary


```python
multid = {'school': 'DMS',
          'students_details': {
              1001: {
                  "name": "Mayank",
                  "age": 41
              },
              1002: {
                  "name" : "Vishal Saxena",
                  "age": 42
              },
              1003: {
                  "name": "Rajeev Chaturvedi",
                  "age": 41
              }
          }
        }
print(multid)
```

    {'school': 'DMS', 'students_details': {1001: {'name': 'Mayank', 'age': 41}, 1002: {'name': 'Vishal Saxena', 'age': 42}, 1003: {'name': 'Rajeev Chaturvedi', 'age': 41}}}



```python
print(multid['students_details'][1001])
print(multid['students_details'][1002]['name'])
```

    {'name': 'Mayank', 'age': 41}
    Vishal Saxena



```python
try:
    print(multid['students_details'][1004]['name'])
except Exception as e:
    print("Error:", e)
```

    Error: 1004



```python
try:
    print(multid['students_details'].get(1004, "None"))
except Exception as e:
    print("Error:", e)
```

    None



```python
multid = {'school': 'DMS',
          'students_details': {
              "students": 
                  [
                      "Mayank",
                      "Vishal",
                      "Rajeev"
                  ]
          }}
print(multid)
```

    {'school': 'DMS', 'students_details': {'students': ['Mayank', 'Vishal', 'Rajeev']}}



```python
dupli = {
    "meme" : "mjmj",
    "test" : "TESt value",
    "meme" : "wewe"
}

print(dupli)
for k in dupli:
    print(k)
```

    {'test': 'TESt value', 'meme': 'wewe'}
    test
    meme



```python
# Matrix in form of string
matrix = '''0 0 0 0 0 0 0 0 0 0 0 0
9 0 0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 4 0 0
0 0 0 0 0 0 0 3 0 0 0 0
0 0 0 0 0 0 5 0 0 0 0 0
0 0 0 0 6 0 0 0 0 0 0 0'''

mat = {}

# split the matrix in lines
for row, line in enumerate(matrix.splitlines()):

    # Splits the line int cols
    for col, column in enumerate(line.split()):

        column = int(column)
        # Places the column in the result,
        # if it is differente from zero
        if column:
            mat[row, col] = column

print (mat)
# The counting starts with zero
print ('Complete matrix size:', (row + 1) * (col + 1))
print ('Sparse matrix size:', len(mat))
```

    {(5, 4): 6, (3, 7): 3, (1, 0): 9, (4, 6): 5, (2, 9): 4}
    Complete matrix size: 72
    Sparse matrix size: 5


> **NOTE**: One can create dictionary using the following methods as well


```python
names = dict(mayank="johri", ashwini="johri", Rahul="Johri")
print(names)
```

    {'mayank': 'johri', 'ashwini': 'johri', 'Rahul': 'Johri'}



```python
names = dict([("mayank","johri"), ("ashwini", "johri"), ("Rahul","Johri")])
print(names)
```

    {'ashwini': 'johri', 'mayank': 'johri', 'Rahul': 'Johri'}


Lets check below two examples and see what is happening


```python
d = dict()
d[10.1] = "TEST"
d[10] = "test"  
d[10.5] = "really testing"
d[20] = "Testing completed"
print(d)
```

    {10.1: 'TEST', 10.5: 'really testing', 20: 'Testing completed', 10: 'test'}



```python
d = dict()
d[10.0] = "TEST"
d[10] = "test"  
d[10.5] = "really testing"
d[20] = "Testing completed"
print(d)
```

    {10.0: 'test', 20: 'Testing completed', 10.5: 'really testing'}


> **NOTE**: Dictionaries are implemented with a hash table and hash of 10.0 and 10 are same, thus  10.0 and 10 keys of dict are same and only one key/value pair is shown for them. Please check the url for details.   
> - https://stackoverflow.com/questions/32209155/why-can-a-floating-point-dictionary-key-overwrite-an-integer-key-with-the-same-v/32211042#32211042


```python
hash(10.0) == hash(10)
```




    True


