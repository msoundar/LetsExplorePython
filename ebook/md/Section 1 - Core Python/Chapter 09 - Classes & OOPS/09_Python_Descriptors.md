
## Python descriptors

Python descriptors are used to create `managed attributes`. Managed Attributes has many benefits other normal attributes, such as 
- Protect attribute value from changing
- Auto updation of depending attributes (in `circle` updating `area` updates `radius` and vice verses) 
- Custom Updation of attribute (e.g. Temprature C to F or F to C etc)

### Descriptors protocol

Descriptors are similar to Java's `getters` & `setters`. Python 

### Need of Descriptors

### Creating descriptors

### Creating descriptors using class methods

#### `__set__`

#### `__get__`

#### `__delete__`

### Creating descriptors using property type

#### Creating descriptors using property decorators

### Creating descriptors at run time

### Binding new functions to existing class and objects


```python
def bind(instance, func, asname):
    setattr(instance,
            asname,
            func.__get__(instance, instance.__class__))

class A:
    pass

def logging(self, logmsg):
    print(logmsg)

# Binding instance to object

a = A()

bind(a, logging, 'logging')
a.logging(":test")
print(dir(a))
print(dir(A))

print("~^"*20)
# Binding function to Class 
bind(A, logging, 'logging')
b = A()
print(dir(b))
print(dir(A))
```

    :test
    ['__class__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', 'logging']
    ['__class__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__']
    ~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^
    ['__class__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', 'logging']
    ['__class__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', 'logging']

