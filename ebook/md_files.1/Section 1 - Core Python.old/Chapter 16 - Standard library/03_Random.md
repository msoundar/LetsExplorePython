
## Random

### `choices`: A weighted version of random
> Python >= 3.6


```python
from random import choices

lnct_few_friends = ["Jyoti Pancholi", 
                    "Amit Shrivastava", 
                    "Mukesh Bansal", 
                    "Preeti Saraswat", 
                    "Manish Nandle"]
list_of_prob = [0.2, 0.1, 0.3, 0.2, 0.2]
prob = choices(lnct_few_friends, 
               weights=list_of_prob, 
               k=10)

print(prob)
print("~^"*10)
for name in lnct_few_friends:
    print(name, prob.count(name))
```

    ['Jyoti Pancholi', 'Amit Shrivastava', 'Manish Nandle', 'Mukesh Bansal', 'Preeti Saraswat', 'Jyoti Pancholi', 'Preeti Saraswat', 'Jyoti Pancholi', 'Amit Shrivastava', 'Mukesh Bansal']
    ~^~^~^~^~^~^~^~^~^~^
    Jyoti Pancholi 3
    Amit Shrivastava 2
    Mukesh Bansal 2
    Preeti Saraswat 2
    Manish Nandle 1


Lets try some graphs on them


```python
from random import choices
import matplotlib.pyplot as plt

lnct_few_friends = ["X", "Jyoti Pancholi", 
                    "Amit Shrivastava", 
                    "Mukesh Bansal", 
                    "Preeti Saraswat", 
                    "Manish Nandle"]
list_of_prob = [0.05, 0.15, 0.1, 0.3, 0.1, 0.3]
d = {}
for i in range(10):
    a = {}
    lst = choices(lnct_few_friends, weights=list_of_prob, k=10)
    for name in set(lnct_few_friends):
        a[name] = lst.count(name)
    d[i] = a

import matplotlib.pyplot as plt
from  matplotlib import ticker
import math
plt.xticks(rotation=90)
for key, val in d.items():
    plt.plot(val.keys() ,val.values(), marker="x")
```


![png](03_Random_files/03_Random_4_0.png)


In the above graph, you can see that everytime, "X" were the lowest and "Mukesh" & "Manish" were the highests names in the created list and more I increase the value of `k` more prominent the graph becomes as shown in the below code and graph


```python
from random import choices
import matplotlib.pyplot as plt

lnct_few_friends = ["X", "Jyoti Pancholi", "Amit Shrivastava", "Mukesh Bansal", "Preeti Saraswat", "Manish Nandle"]
list_of_prob = [0.05, 0.15, 0.1, 0.3, 0.1, 0.3]
d = {}
for i in range(10):
    a = {}
    lst = choices(lnct_few_friends, weights=list_of_prob, k=900)
    for name in set(lnct_few_friends):
        a[name] = lst.count(name)
    d[i] = a

import matplotlib.pyplot as plt
from  matplotlib import ticker
import math
plt.xticks(rotation=90)
for key, val in d.items():
    plt.plot(val.keys() ,val.values(), marker="o")
```


![png](03_Random_files/03_Random_6_0.png)

