
# Chapter 0 - Who & Why
---

This tutorial is created for the sole purpose for provide a base document for my training programs. I am releasing it on the net in hope of people find it useful. 



## Who 
---


I am Mayank Johri. I am a among many things, primarily a student and opensource developer.

Few of my opensource projects can be found at 

- https://sourceforge.net/projects/softgridhelper/
- https://gitlab.com/mayankjohri

### My history with Python

Till 2005, VB.NET & C were my primary programming languages. In early 2005, due to certain personal & political (read MS) reasons, I started searching for my next primary language for which my requirements were as follows

- Opensource language (so I don't have to pay, no licensing issues, no change in language without some workaround for issues)
- Easy to learn (Never had too much time)
- can be compiled to executables
- GUI programming should be possible (wx)
- Relatively decent execution speed
- Decent support and documentation

After about six months, and trying almost most the programming languages from (https://en.wikipedia.org/wiki/List_of_programming_languages), I found that python language suite most of my requirements and thus it became my first choice of programming language.

Other langauges which came near were, "Ruby", "FreeBasic", "C" & "lua". I liked "FreeBasic" and also migrated one crutial utility of `SoftGridHelper` to it from Python/.Net, since, that utility was to always running on user machines and was to be installed on nearly every machine on the client infrastructure (excluding servers) and "FreeBasic" provided me an exeuctable with in few "kb's" and without horrible dependency of `.NET` versions.
