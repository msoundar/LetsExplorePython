
# Starting Point
------------

In this section we will cover few items, without which out training can't continue. We will be covering only briefly those topics, as they will be covered in details later.

Lets start out discussion with few **Built-in Functions**.

## Basic functions

* `print`: Print function provides a method to display data on the standard output, which is in most default cases is the console. It inserts spaces between expressions that are received as parameters, and a newline character at the end, unless requested otherwise.


```python
print("!!! स्वागतम् !!!")
print("!!! Welcome !!!")
print("!!! Willkommen !!!")
```

    !!! स्वागतम् !!!
    !!! Welcome !!!
    !!! Willkommen !!!



```python
print("Hello", ",", "What are you doing")
```

    Hello , What are you doing


* `pass`: This function does nothing and acts only as a placeholder for the implementation in future.


```python
pass
```

* `id`: Return the “identity” of an object. This is an integer (or long integer) which is guaranteed to be unique and constant for this object during its lifetime. Two objects with non-overlapping lifetimes may have the same id() value.

> **NOTE**: CPython implementation detail: This is the address of the object in memory.

* `input`: If the prompt argument is present, it is written to standard output without a trailing newline. The function then reads a line from input, converts it to a string (stripping a trailing newline), and returns that. When EOF is read, EOFError is raised


```python
name = input("!!! Welcome to funzone!!!,\nPlease enter your name: ")
print("Hello", name, ", how are you doing?" )
```

    !!! Welcome to funzone!!!,
    Please enter your name: ashwani
    Hello ashwani , how are you doing?

