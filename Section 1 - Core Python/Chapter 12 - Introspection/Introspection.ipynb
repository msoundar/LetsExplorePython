{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introspection"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "__Introspection__ or __reflection__ is the ability of software to identify and report their own internal structures, such as types, variable scope, methods and attributes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Python provides multiple methods for introspection of the code. Few of them are listed below. \n",
    "\n",
    "| Function | Returns|\n",
    "|----------|---------------------|\n",
    "| `type(object)` |  The type/class of the object |\n",
    "| `id(object)` |  object identifier |\n",
    "| `locals()` |  dictionary containing local variables with values |\n",
    "| `globals()` |  dictionary containing global variables with values  |\n",
    "| `vars(object)` |  object symbols dictionary |\n",
    "| `len(object)`  |  size of an object/collection |\n",
    "| `dir(object)` |  A list of object attributes |\n",
    "| `help(object)` |  Object's doc strings |\n",
    "| `repr(object)` |  Object representation or `__repr__` function |\n",
    "| `isinstance(object, class)` |  True if object is derived from class |\n",
    "| `issubclass(subclass, class)` |  True if object inherits the class |"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### `type`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It returns the type of the data being introspected as shown in the below example"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 38,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      " 1                   : <class 'int'>\n",
      " 10.2                : <class 'float'>\n",
      " (3+4j)              : <class 'complex'>\n",
      " True                : <class 'bool'>\n",
      " 0                   : <class 'int'>\n",
      " ओ३म्                : <class 'str'>\n",
      " ['t', 'l']          : <class 'list'>\n"
     ]
    }
   ],
   "source": [
    "data_group = [\n",
    "    1,    10.2,   3 + 4j,\n",
    "    True, 0,      \"ओ३म्\",\n",
    "    [\"t\", \"l\"]\n",
    "]\n",
    "for data in data_group:\n",
    "    print(\" {:20}: {}\".format(str(data), type(data)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### `id(object)`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Return the “identity” of an object. This is an integer (or long integer) which is guaranteed to be unique and constant for this object during its lifetime. Two objects with non-overlapping lifetimes may have the same id() value.\n",
    "\n",
    "> **NOTE**: CPython implementation detail: This is the address of the object in memory."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "10 140196247471424\n",
      "20 140196247471744\n"
     ]
    }
   ],
   "source": [
    "a, b = 10, 20\n",
    "print(a, id(a))\n",
    "print(b, id(b))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### `vars(object)`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`vars` behave like `__dict__`, but with one exception, that it can be used to get other objects by passing them as parameters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{'a': 'Apple'} {'a': 'Apple'}\n",
      "{'a': 'Apple', 'color': 'Green'} {'a': 'Apple', 'color': 'Green'}\n"
     ]
    }
   ],
   "source": [
    "class Apple(object):\n",
    "    def __init__(self):\n",
    "        self.a = \"Apple\"\n",
    "\n",
    "a = Apple()\n",
    "\n",
    "print(vars(a), a.__dict__)\n",
    "a.color = \"Green\"\n",
    "print(vars(a), a.__dict__)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### `vars` acting as `locals`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When we don't provide any argument to `vars`, it acts as `locals` and returns local variables."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{'d': 'Green Apple', 'self': <__main__.Apple object at 0x7f81ec3bb518>}\n"
     ]
    }
   ],
   "source": [
    "class Apple(object):\n",
    "    def __init__(self):\n",
    "        self.a = \"Apple\"\n",
    "        \n",
    "    def intro(self):\n",
    "        d = \"Green Apple\"\n",
    "        return vars()\n",
    "        \n",
    "a = Apple()\n",
    "print(a.intro())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 33,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{'d': 'Green', 'color': 'Green'}\n"
     ]
    }
   ],
   "source": [
    "def intro(color):\n",
    "    d = color\n",
    "    return vars()\n",
    "        \n",
    "print(intro(\"Green\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The object identifier is a unique number that is used by the interpreter for identifying the objects internally.\n",
    "\n",
    "    Example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Representation: <class '__main__.Apple'>\n",
      "Representation: ['', 'class Apple(object):\\n    def __init__(self):\\n        self.a = \"Apple\"\\n\\na = Apple()\\n\\nprint(vars(a), a.__dict__)', 'class Apple(object):\\n    def __init__(self):\\n        self.a = \"Apple\"\\n\\na = Apple()\\n\\nprint(vars(a), a.__dict__)', 'class Apple(object):\\n    def __init__(self):\\n        self.a = \"Apple\"\\n\\na = Apple()\\n\\nprint(vars(a), a.__dict__)\\na.color = \"Green\"\\nprint(vars(a), a.__dict__)', \"import types\\n\\ns = ''\\nif isinstance(s, types.StringType):\\n    print 's is a string.'\", 'l = [1, 2, 3, 4, 5]\\nprint(len(l))', 'l = [1, 2, 3, 4, 5]\\nprint(len(l))', 'l = {\"test\": \"tes\", \"e\": \"d\"}\\nprint(len(l))', \"# about global objects in the program\\n\\nfrom types import ModuleType\\n\\ndef info(n_obj):\\n\\n    # Create a referênce to the object\\n    obj = globals()[n_obj]\\n\\n    # Show object information \\n    print('Name of object:', n_obj)\\n    print('Identifier:', id(obj))\\n    print('Typo:', type(obj))\\n    print('Representation:', repr(obj))\\n\\n    # If it is a module\\n    if isinstance(obj, ModuleType):\\n        print( 'itens:')\\n        for item in dir(obj):\\n            print (item)\\n    print\\n\\n# Showing information\\nfor n_obj in dir()[:10]: # The slice [:10] is used just to limit objects\\n    info(n_obj)\", \"# about global objects in the program\\n\\nfrom types import ModuleType\\n\\ndef info(n_obj):\\n\\n    # Create a referênce to the object\\n    obj = globals()[n_obj]\\n\\n    # Show object information \\n#     print('Name of object:', n_obj)\\n#     print('Identifier:', id(obj))\\n#     print('Typo:', type(obj))\\n    print('Representation:', repr(obj))\\n\\n#     # If it is a module\\n#     if isinstance(obj, ModuleType):\\n#         print( 'itens:')\\n#         for item in dir(obj):\\n#             print (item)\\n    print\\n\\n# # Showing information\\n# for n_obj in dir()[:10]: # The slice [:10] is used just to limit objects\\n#     info(n_obj)\", \"# about global objects in the program\\n\\nfrom types import ModuleType\\n\\ndef info(n_obj):\\n\\n    # Create a referênce to the object\\n    obj = globals()[n_obj]\\n\\n    # Show object information \\n#     print('Name of object:', n_obj)\\n#     print('Identifier:', id(obj))\\n#     print('Typo:', type(obj))\\n    print('Representation:', repr(obj))\\n\\n#     # If it is a module\\n#     if isinstance(obj, ModuleType):\\n#         print( 'itens:')\\n#         for item in dir(obj):\\n#             print (item)\\n    print\\n\\n# # Showing information\\nfor n_obj in dir()[:10]: # The slice [:10] is used just to limit objects\\n    info(n_obj)\"]\n",
      "Representation: <class 'module'>\n",
      "Representation: {}\n",
      "Representation: ''\n",
      "Representation: ''\n",
      "Representation: ''\n",
      "Representation: <module 'builtins' (built-in)>\n",
      "Representation: <module 'builtins' (built-in)>\n",
      "Representation: 'Automatically created module for IPython interactive environment'\n"
     ]
    }
   ],
   "source": [
    "# about global objects in the program\n",
    "\n",
    "from types import ModuleType\n",
    "\n",
    "def info(n_obj):\n",
    "\n",
    "    # Create a referênce to the object\n",
    "    obj = globals()[n_obj]\n",
    "\n",
    "    # Show object information \n",
    "#     print('Name of object:', n_obj)\n",
    "#     print('Identifier:', id(obj))\n",
    "#     print('Typo:', type(obj))\n",
    "    print('Representation:', repr(obj))\n",
    "\n",
    "#     # If it is a module\n",
    "#     if isinstance(obj, ModuleType):\n",
    "#         print( 'itens:')\n",
    "#         for item in dir(obj):\n",
    "#             print (item)\n",
    "    print\n",
    "\n",
    "# # Showing information\n",
    "for n_obj in dir()[:10]: # The slice [:10] is used just to limit objects\n",
    "    info(n_obj)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Python also has a module called *types*, which has the definitions of the basic types of the interpreter.\n",
    "\n",
    "Example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "ename": "AttributeError",
     "evalue": "module 'types' has no attribute 'StringType'",
     "output_type": "error",
     "traceback": [
      "\u001b[1;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[1;31mAttributeError\u001b[0m                            Traceback (most recent call last)",
      "\u001b[1;32m<ipython-input-12-afd900e1e04c>\u001b[0m in \u001b[0;36m<module>\u001b[1;34m\u001b[0m\n\u001b[0;32m      2\u001b[0m \u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m      3\u001b[0m \u001b[0ms\u001b[0m \u001b[1;33m=\u001b[0m \u001b[1;34m''\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[1;32m----> 4\u001b[1;33m \u001b[1;32mif\u001b[0m \u001b[0misinstance\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0ms\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0mtypes\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0mStringType\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m:\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0m\u001b[0;32m      5\u001b[0m     \u001b[0mprint\u001b[0m\u001b[1;33m(\u001b[0m\u001b[1;34m's is a string.'\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n",
      "\u001b[1;31mAttributeError\u001b[0m: module 'types' has no attribute 'StringType'"
     ]
    }
   ],
   "source": [
    "import types\n",
    "\n",
    "s = ''\n",
    "if isinstance(s, types.StringType):\n",
    "    print('s is a string.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## `repr`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<__main__.Users object at 0x00000000056282E8>\n"
     ]
    }
   ],
   "source": [
    "class Users(object):\n",
    "    def __init__(self, username, age):\n",
    "        self.username = username\n",
    "        self.age = age\n",
    "        \n",
    "chandu = Users(\"Chandu Nalluri\", 45)\n",
    "print(repr(chandu))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "This object contains following data\n",
      "        name: Chandu Nalluri, age: 45\n"
     ]
    }
   ],
   "source": [
    "class Users(object):\n",
    "    def __init__(self, username, age):\n",
    "        self.username = username\n",
    "        self.age = age\n",
    "    \n",
    "    def __repr__(self):\n",
    "        return \"\"\"This object contains following data\n",
    "        name: {}, age: {}\"\"\".format(self.username, self.age)\n",
    "        \n",
    "chandu = Users(\"Chandu Nalluri\", 45)\n",
    "print(repr(chandu))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## `len`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`len` will return the number of elements present in the collection."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "5\n"
     ]
    }
   ],
   "source": [
    "# list\n",
    "l = [1, 2, 3, 4, 5]\n",
    "print(len(l))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2\n"
     ]
    }
   ],
   "source": [
    "# dictionary\n",
    "l = {\"test\": \"tes\", \"e\": \"d\"}\n",
    "print(len(l))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Through introspection, it is possible to determine the fields of a database table, for example.\n",
    "\n",
    "Inspect\n",
    "-------\n",
    "The module *inspect* provides a set of high-level functions that allow for introspection to investigate types, collection items, classes, functions, source code and the runtime stack of the interpreter.\n",
    "\n",
    "Example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Object: <module 'posixpath' from '/home/csig/env/teste/lib/python2.7/posixpath.pyc'>\n",
      "Class? True\n",
      "Member: _joinrealpath abspath basename commonprefix dirname exists expanduser expandvars getatime getctime getmtime getsize isabs isdir isfile islink ismount join lexists normcase normpath realpath relpath samefile sameopenfile samestat split splitdrive splitext walk\n"
     ]
    }
   ],
   "source": [
    "import os.path\n",
    "# inspect: \"friendly\" introspection module\n",
    "import inspect\n",
    "\n",
    "print 'Object:', inspect.getmodule(os.path)\n",
    "\n",
    "print 'Class?', inspect.isclass(str)\n",
    "\n",
    "# Lists all functions that exist in \"os.path\"\n",
    "\n",
    "print 'Member:',\n",
    "\n",
    "for name, struct in inspect.getmembers(os.path):\n",
    "\n",
    "    if inspect.isfunction(struct):\n",
    "        print name, "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The functions that work with the stack of the interpreter should be used with caution because it is possible to create cyclic references (a variable that points to the stack item that has the variable itself). The existence of references to stack items slows the destruction of the items by the garbage collector of the interpreter."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [],
   "source": [
    "import inspect\n",
    "\n",
    "\n",
    "def myself():\n",
    "    return inspect.stack()[1][3]\n",
    "\n",
    "def parent_function():\n",
    "    return inspect.stack()[2][3]\n",
    "\n",
    "\n",
    "def power(expo):\n",
    "    print(\"I am at {name}, {parent}\".format(name=myself(), parent=parent_function()))\n",
    "    def inner(num):\n",
    "        print(\"I am at {name}, {parent}\".format(name=myself(), parent=parent_function()))\n",
    "        return num**expo\n",
    "    return inner\n",
    "              \n",
    "\n",
    "def test_power(a, b):\n",
    "    p = power(a)\n",
    "    p(b)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "I am at power, <module>\n",
      "I am at inner, <module>\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "10000000000"
      ]
     },
     "execution_count": 23,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "d = power(10)\n",
    "d(10)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "I am at power, test_power\n",
      "I am at inner, test_power\n"
     ]
    }
   ],
   "source": [
    "test_power(10, 5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reference"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- http://stefaanlippens.net/python_inspect/"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
