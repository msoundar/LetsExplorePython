
## Why Inheritance

 
- Gets rid of Code duplication 


## Single Inheritance

In single inheritance, any client class inherits from only one parent class. Lets look at the below example which contains `Pen` as parent and `BallPen` & `InkPen` as clildren of it. 


```python
class Pen(object):
    """
    Should implement all the common features 
    """
    def __init__(self, size, name):
        self.name = name
        self.size = size
    
    def set_name(self, name):
        self.name = name


class BallPen(Pen):
    """
    Only the unique features should be implemented in child classes.
    """
    
    def __init__(self, size, name, color):
        self.color = color
        super().__init__(size, name)
    
    def set_color(self, color):
        self.color = color


class InkPen(Pen):
    def __init__(self, size, name, cart_type):
        self.cart = cart_type
        super().__init__(size, name)
```

`BallPen` & `InkPen` both are initializing the parent class using `super().__init(size, name)` function. Now lets create few objects of both,


```python
pb = BallPen(10, "Renolds", "Green")
print(pb.name)
pb.set_name("cello")
print(pb.name)
print(pb.__dict__)
```

    Renolds
    cello
    {'color': 'Green', 'name': 'cello', 'size': 10}



```python
ip = InkPen(size="10 cm", cart_type="2 MM", name="Renolds")
print(ip.name)
ip.set_name("cello")
print(ip.name)
print(ip.__dict__)
```

    Renolds
    cello
    {'cart': '2 MM', 'name': 'cello', 'size': '10 cm'}



```python
class grand_parent(object):
    def __init__(self, middle_name):
        print("grand_parent init")
        self.__middle_name = middle_name
        
    def middle_name(self, middle_name):
        self.__middle_name = middle_name
        return self.__middle_name
```

Lets create a `parent` class which inherits `grand_parent` class, note we have used `super().__init_(middle_name)` to set middle name using parents function `middle_name`. 


```python
class parent(grand_parent):
    def __init__(self, middle_name, surname):
        print("parent init")
        self.__surname = surname
        super().__init__(middle_name)
    
    def surname_name(self):
        return self.__surname
```

Now lets create the `student` which inherits `parent` class. Check its init also. 


```python
class student(parent):
    def __init__(self, name, middle_name, surname):
        print("student init")
        self.name = name
        super().__init__(middle_name, surname)
```


```python
mohan = student("Venkat", "kumar", "Mohan")
```

    student init
    parent init
    grand_parent init


Check the order of `init`'s being called. 


```python
print(mohan.middle_name)
```

    <bound method parent.middle_name of <__main__.student object at 0x7f9df848b160>>



```python
mohan.middle_name = "KUMAR"
print(mohan.middle_name)
```

    KUMAR


Now lets create the same classes without init functions, and see what happens


```python
class grand_parent:
    def __init__(self, middle_name):
        print("grand_parent init")
        self.__middle_name = middle_name
        
    def middle_name(self, middle_name):
        self.__middle_name = middle_name
        return self.__middle_name

class parent(grand_parent):
    def __init__(self, middle_name, surname):
        print("parent init")
        self.__surname = surname
    
    def middle_name(self):
        return self.__middle_name
    
    
class student(parent):
    def __init__(self, name, middle_name, surname):
        print("student init")
        self.name = name
```


```python
mohan = student("Venkat", "kumar", "Mohan")
```

    student init



```python
try:
    print(mohan.middle_name())
except Exception as e:
    print(e)
```

    'student' object has no attribute '_parent__middle_name'



```python
class A(object):
    def test(self, a):
        print("test", a)
        
class B(A):
    def testing(self, b):
        print("testing", b)
    
class C(B):
    def test_c(self, c):
        super().test(c)
        
c = C()
c.test_c("Hello")
```

    test Hello



```python
class A(object):
    def test(self, a):
        print("test", a)
        
class B(A):
    def testing(self, b):
        print("testing", b)
    
class C(B):
    def test_c(self, c):
        self.test(c)
        
c = C()
c.test_c("Hello")
```

    test Hello


### Why `super` when `self` can also do similar task

super can be called to explicitly call functions as shown in the below code. we are directly calling `A.test` and skipping `B.test` 


```python
class A(object):
    def test(self, a):
        print("test A", a)
        
class B(A):
    def test(self, b):
        print("test B", b)
    
class C(B):
    def test_c(self, c):
        super(B, self).test(c)
        
c = C()
c.test_c("Hello")
```

    test A Hello



```python
class A(object):
    def test(self, a):
        print("test A", a)
        
class B(A):
    def test(self, b):
        print("test B", b)
    
class C(B):
    def test_c(self, c):
        self.test(c)
        
c = C()
c.test_c("Hello")
```

    test B Hello



```python
class A(object):
    def test(self, a):
        print("test A", a)
        
class B(A):
    def test(self, b):
        print("test B", b)
    
class C(B):
    def test(self, c):
        super(B, self).test(c)
        
class D(C):
    def test(self, txt):
        super(C, self).test(txt)

    def test_b(self, txt):
        super(B, self).test(txt)
    
    def test_c(self, txt):
        super().test(txt)
d = D()
d.test("Testing")
d.test_b("Testing b")
d.test_c("testing c")
```

    test B Testing
    test A Testing b
    test A testing c



```python
# NOTE: python 2 has issues with Super , get it also documented here
```


```python
# class A(object):
#     def test(self):
#         print("In A")

# class B(object):
#     pass

# A.register(B)

# b = B()
# b.test()
```
