
# The inner class
---
An inner class or nested class is a defined entirely within the body of another class  Let us do an example:


```python
class Human:

    def __init__(self, writing_hand):
        self.name = 'Guido'
        self.head = self.Head()
        self.left_hand = self.Hand()
        self.right_hand = self.Hand()
        self.writing_hand = writing_hand

    class Head:
        def talk(self):
            return 'talking...'
    
    class Hand:
        def writing(self, txt):
            print(txt)

    def lets_talk(self):
        return self.head.talk();
    
    def lets_write(self, txt):
        if(self.writing_hand.lower() == 'left'):
            self.left_hand.writing(txt)
        else:
            self.right_hand.writing(txt)
        
guido = Human("right")
print(guido.name)
print(guido.head.talk())
# Bad example misusing the methods
guido.Hand.writing(guido, "hello")
print("*"*20)
guido.right_hand.writing("Ja")
guido.lets_write("World")
```

    Guido
    talking...
    hello
    ********************
    Ja
    World



```python
hand = Human.Hand()
hand.writing("Welcome ~~~")
```

    Welcome ~~~


### when to use 
