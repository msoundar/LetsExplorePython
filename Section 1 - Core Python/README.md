
# Preface
---

This book has been created as part of reference material for learning Python languages.

The entire training material has been divided into three sections

* Core Python
* Advance Python &
* Machine Learning

This e-book will try to cover only "core python" and I am in a process of finishing the other two e-books. I am planning to release the First (draft) versions of them with following time lines. 
- "Advance Python" might be released sometime in Oct 2018
- "Machine Learning" by December 2018.

This section is specifically designed for everyone who wish to work/learn Python language.

## Reference

Below is the list of reference materials which I use, when conducting Python training and also materials from my Web site that are intended for self-instruction.

Most of the material has been collected from Internet. Primary source of the information in the book has been from the followings.

1. My Understanding of Python

2. Python Manual http://docs.python.org

3. [https://github.com/ricardoduarte/python-for-developers](https://github.com/ricardoduarte/python-for-developers)

4. [http://www.davekuhlman.org/python\_book\_01.pdf](http://www.davekuhlman.org/python_book_01.pdf)

5. Wikipedia

6. Wiki-books, etc
