
## Lists

Lists are **collections** of **heterogeneous objects**, which can be of any type, including other lists.

Lists in the Python are mutable and thus can be elements added, removed and updated. Lists can be sliced similar to *strings*.

Syntax of list creation is as follows 

**Syntax**:

```python
list_name = [a, b, ..., z]
```

Example:


```python
fruits = ['Apple', 'Mango', 'Grapes', 'Jackfruit', 
          'Apple', 'Banana', 'Grapes', [1, 3 + 4j]]
```

In the above example, we have multiple strings as elements, you will also observe that multiple instances of duplicate data such as `Apple` & `Grapes` are present in the list. 

Lets cover few most common operations which can be performed on the list elements or list. 

### Inserting Elements


```python
fruits.insert(3,[ "Water Melon"])
print(fruits)
```

    ['Apple', 'Mango', 'Grapes', ['Water Melon'], 'Jackfruit', 'Apple', 'Banana', 'Grapes', [1, (3+4j)]]



```python
# !! Gotcha's 
fr = fruits
print(id(fr))
print(id(fruits))
```

    2255742514312
    2255742514312



```python
print(fruits[2])
```

    Grapes



```python
ft1 = list(fruits)
print(id(ft1))
print(id(fruits))
print(id(ft1[2]))
print(id(fruits[2]))
```

    140478987045448
    140478987775624
    140478986941976
    140478986941976



```python
ft1 = fruits[:]
print(id(ft1))
print(id(fruits))
print(id(ft1[2]))
print(id(fruits[2]))
```

    140478986909000
    140478987775624
    140478986941976
    140478986941976



```python
fruits = ['kiwi', 'Apple', 'Camel']
c = fruits[2]
print(c)
c = c[-1] + c[1:-1] + c[0]
print(c)
fruits[2] = c
print(fruits)
```

    Camel
    lameC
    ['kiwi', 'Apple', 'lameC']


### Appending Elements


```python
fruits.append('Camel')
print(fruits)
fruits.append(['kiwi', 'Apple', 'Camel'])
print(fruits)
```

    ['Apple', 'Mango', 'Grapes', ['Water Melon'], 'Jackfruit', 'Apple', 'Banana', 'Grapes', [1, (3+4j)], 'Camel', ['kiwi', 'Apple', 'Camel'], 'Camel']
    ['Apple', 'Mango', 'Grapes', ['Water Melon'], 'Jackfruit', 'Apple', 'Banana', 'Grapes', [1, (3+4j)], 'Camel', ['kiwi', 'Apple', 'Camel'], 'Camel', ['kiwi', 'Apple', 'Camel']]



```python
fruits.append(('kiwi', 'Apple', 'Camel'))
print(fruits)
```

    ['Apple', 'Mango', 'Grapes', ['Water Melon'], 'Jackfruit', 'Apple', 'Banana', 'Grapes', [1, (3+4j)], 'Camel', ['kiwi', 'Apple', 'Camel'], 'Camel', ['kiwi', 'Apple', 'Camel'], ('kiwi', 'Apple', 'Camel')]


### Extending Elements


```python
fruits.extend(['kiwi', 'Apple', 'Camel'])
print(fruits)
```

    ['Apple', 'Mango', 'Grapes', ['Water Melon'], 'Jackfruit', 'Apple', 'Banana', 'Grapes', [1, (3+4j)], 'Camel', ['kiwi', 'Apple', 'Camel'], 'Camel', ['kiwi', 'Apple', 'Camel'], ('kiwi', 'Apple', 'Camel'), 'kiwi', 'Apple', 'Camel']



```python
fruits.extend(('kiwi', ('Apple', 'Camel')))
print(fruits)
```

    ['Apple', 'Mango', 'Grapes', ['Water Melon'], 'Jackfruit', 'Apple', 'Banana', 'Grapes', [1, (3+4j)], 'Camel', ['kiwi', 'Apple', 'Camel'], 'Camel', ['kiwi', 'Apple', 'Camel'], ('kiwi', 'Apple', 'Camel'), 'kiwi', 'Apple', 'Camel', 'kiwi', 'Apple', 'Camel', 'kiwi', ('Apple', 'Camel'), 'kiwi', ('Apple', 'Camel')]


> NOTE: Only one level of extending happens, apple and camel are still in sub-list


```python
fruits.extend(['kiwi', ['Apple', 'Camel']])
print(fruits)
```

    ['Apple', 'Mango', 'Grapes', ['Water Melon'], 'Jackfruit', 'Apple', 'Banana', 'Grapes', [1, (3+4j)], 'Camel', ['kiwi', 'Apple', 'Camel'], 'Camel', ['kiwi', 'Apple', 'Camel'], ('kiwi', 'Apple', 'Camel'), 'kiwi', 'Apple', 'Camel', 'kiwi', 'Apple', 'Camel', 'kiwi', ('Apple', 'Camel'), 'kiwi', ('Apple', 'Camel'), 'kiwi', ['Apple', 'Camel']]


## Removing


```python
## Removing the second instance of Grapes
x = 0
y = 0
for fruit in fruits:
    if x == 1 and fruit == 'Grapes':
#          del (fruits[y])
        fr = fruits.pop(y)
        print(fr)
    elif fruit == 'Grapes':
        x = 1
    y +=1
    
print(fruits)   
```

    Grapes
    ['Apple', 'Mango', 'Grapes', ['Water Melon'], 'Jackfruit', 'Apple', 'Banana', [1, (3+4j)], 'Camel', ['kiwi', 'Apple', 'Camel'], 'Camel', ['kiwi', 'Apple', 'Camel'], ('kiwi', 'Apple', 'Camel'), 'kiwi', 'Apple', 'Camel', 'kiwi', 'Apple', 'Camel', 'kiwi', ('Apple', 'Camel'), 'kiwi', ('Apple', 'Camel'), 'kiwi', ['Apple', 'Camel']]



```python
fruits.remove('Grapes')
```

## Ordering 


```python
# These will work on only homogeneous list and will fail for heterogeneous
try:
    fruits.sort()
    print(fruits)
except Exception as e:
    print(e)
```

    '<' not supported between instances of 'list' and 'str'



```python
fruits = ['Apple', 'Mango', 'Grapes', 'Jackfruit', 'Apple', 'Banana', 'Grapes']

try:
    fruits.sort()
    print(fruits)
except Exception as e:
    print(e)
```

    ['Apple', 'Apple', 'Banana', 'Grapes', 'Grapes', 'Jackfruit', 'Mango']



```python
fruits = ['Apple', 'Mango', 'Grapes', 'Jackfruit', 'Apple', 'Banana', 'Grapes']
f = sorted(fruits)
print("new list:", f, "\nold list:", fruits)
```

    new list: ['Apple', 'Apple', 'Banana', 'Grapes', 'Grapes', 'Jackfruit', 'Mango'] 
    old list: ['Apple', 'Mango', 'Grapes', 'Jackfruit', 'Apple', 'Banana', 'Grapes']



```python
fruits = ['Apple', 'Mango', 'Grapes', 'Jackfruit', 'Apple', 'Banana', 'Grapes']

try:
    fruits.sort(reverse=True)
    print(fruits)
except Exception as e:
    print(e)
```

    ['Mango', 'Jackfruit', 'Grapes', 'Grapes', 'Banana', 'Apple', 'Apple']



```python
help(list.sort)
```

    Help on method_descriptor:
    
    sort(self, /, *, key=None, reverse=False)
        Stable sort *IN PLACE*.
    



```python
lst = [[1, 2], [2, 3], [3, 4], [4, 10], [3, 2]]
lst.sort()
print(lst)
```

    [[1, 2], [2, 3], [3, 2], [3, 4], [4, 10]]


## Inverting


```python
fruits.reverse()
print(fruits)
```

    ['Camel', 'Apple', 'kiwi']



```python
fruits = ['kiwi', 'Apple', 'Camel']
print(fruits[::-1])
```

    ['Camel', 'Apple', 'kiwi']


### `enumerate`


```python
# # # prints with number order
fruits = ['Apple', 'Mango', 'Grapes', 'Jackfruit', 'Banana']
for i, prog in enumerate(fruits):
    print( i + 1, '=>', prog)
```

    1 => Apple
    2 => Mango
    3 => Grapes
    4 => Jackfruit
    5 => Banana


The function `enumerate()` returns a tuple of two elements in each iteration: a sequence number and an item from the corresponding sequence.

The list has a `pop()` method that helps the implementation of queues and stacks:


```python
my_list = ['A', 'B', 'C']
for a, b in enumerate(my_list):
    print(a, b)
```

    0 A
    1 B
    2 C



```python
my_list = ['A', 'B', 'C']
print ('list:', my_list)

# # The empty list is evaluated as false
while my_list:
    # In queues, the first item is the first to go out
    # pop(0) removes and returns the first item 
    print ('Removing', my_list.pop(0), ', remain', len(my_list), my_list)
```

    list: ['A', 'B', 'C']
    Removing A , remain 2 ['B', 'C']
    Removing B , remain 1 ['C']
    Removing C , remain 0 []



```python
my_list.append("G")
# # More items on the list
my_list += ['D', 'E', 'F']
print ('list:', my_list)
```

    list: ['G', 'D', 'E', 'F']


The sort (*sort*) and reversal (*reverse*) operations are performed in the list and do not create new lists.

## Tuples

Similar to lists, but immutable: it's not possible to append, delete or make assignments to the items.

**Syntax:**

    my_tuple = (a, b, ..., z)

The parentheses are optional.

Feature: a tuple with only one element is represented as:

t1 = (1,)

The tuple elements can be referenced the same way as the elements of a list:

    first_element = tuple[0]

Lists can be converted into tuples:

    my_tuple = tuple(my_list)

And tuples can be converted into lists:

    my_list = list(my_tuple)

While tuple can contain mutable elements, these elements can not undergo assignment, as this would change the reference to the object.

Example :


```python
t = (1)
print(type(t))
```

    <class 'int'>



```python
t = (1,)
print(type(t))
```

    <class 'tuple'>



```python
# Converting single element list to a single element tuple
t = tuple([1])
print(type(t))
```

    <class 'tuple'>



```python
# optional `(` `)`
t = 1, 2, 3, 4
print(type(t), t)
```

    <class 'tuple'> (1, 2, 3, 4)



```python
t = ([1, 2], 4)
print(t)
```

    ([1, 2], 4)



```python
print(" :: Error :: ")
try:
    t[0] = 3
    print(t)
except Exception as e:
    print(e)
```

     :: Error :: 
    'tuple' object does not support item assignment



```python
print(" :: Error :: ")
try:
    t[0] = [1, 2, 3]
    print(t)
except Exception as e:
    print(e)
```

     :: Error :: 
    'tuple' object does not support item assignment



```python
t[0].append(3)
print(t)
```

    ([1, 2, 3], 4)



```python
t[0][0] = [1, 2, 3]
print(t)
```

    ([[1, 2, 3], 2, 3], 4)



```python
ta = (1, 2, 3, 4, 5)

for a in ta:
    print (a)
```

    1
    2
    3
    4
    5



```python
ta1 = [1, 2, 3, 4, 5]
for a in ta1:
    print(a)
```

    1
    2
    3
    4
    5


**NOTE**: Tuples are more efficient than conventional lists, as they consume less computing resources (memory) because they are simpler structures the same way *immutable* strings are in relation to *mutable* strings.

### Lists Versus Tuples

Tuples are used to collect an immutable ordered list of elements. This means that to a tuple (**limitation**):

- elements can't be added, thus There’s no append() or extend() method for tuples,
- elements can't be removed, thus Tuples have no remove() or pop() method,

So, if we have a constant set of values and only we will iterate through it than use a tuple instead of a list as It is faster & safer than working with lists, as the tuples contain “write-protect” data.
