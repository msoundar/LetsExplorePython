
# Chapter 0 - Who & Why
---

This tutorial is created for the sole purpose for provide a base document for my training programs. I am releasing it on the net in hope of people find it useful. 



## Who 
---


I am Mayank Johri and since 2000 I have been working in IT domain. In this time I was able to release few opensource projects and books on it. Projects can be found at 

- https://sourceforge.net/projects/softgridhelper/
- https://gitlab.com/mayankjohri

and books at Amazon. 

### My history with Python

Till 2005, VB.NET & C were my primary programming languages. In early 2005, due to certain personal & political (read M$) reasons, I started searching for my next primary language. The language has to meet my following requirements.

- Opensource language (so I don't have to pay, no licensing issues, no change in language without some workaround for issues)
- Easy to learn (Never had too much time)
- can be compiled to executables
- GUI programming should be possible (wx)
- Relatively decent execution speed
- Decent support and documentation

After about six months, and trying almost most the programming languages from (https://en.wikipedia.org/wiki/List_of_programming_languages), I found that python language suite most of my requirements and thus it became my first choice of programming language.

Other langauges which came very near were: "Ruby", "FreeBasic", "C" & "lua". I liked "FreeBasic" and also migrated one crutial utility of `SoftGridHelper` to it from Python/.Net, since, that utility was to always running on user machines and was to be installed on nearly every machine on the client infrastructure (excluding servers) and "FreeBasic" provided me an exeuctable with in few "kb's" and without horrible dependency issues of `.NET` versions and it was used in few corporates without any issue.


```python
# This documented has been created Using
import sys
print(sys.version)
print(sys.version_info)
```

    3.6.5 (default, Sep  3 2018, 08:43:13) 
    [GCC 7.3.1 20180703]
    sys.version_info(major=3, minor=6, micro=5, releaselevel='final', serial=0)

